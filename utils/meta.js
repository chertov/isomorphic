export default function meta() {
    return {
        metaDescription: '',
        metaKeywords: '',
        metaCanonical: '',
        metaTitle: 'my_meta_page_title',
        metaTwitterTitle: '',
        metaTwitterDescription: '',
        metaOgTitle: '',
        metaOgDescription: '',
        metaOgImage: '',
        metaOgUrl: '',
        metaOgSiteName: 'Никамиста — ортопедические накладки Селиванова'
    };
}
