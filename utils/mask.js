/* eslint-disable */
const maskTransitions = {
    '9': { pattern: /\d/ },
    '&': { pattern: /\d|,|\.| / }, // eslint-disable-line no-irregular-whitespace
    '#': { pattern: /\d| / }, // eslint-disable-line no-irregular-whitespace
    'S': { pattern: /\d|\// },
    's': { pattern: /\// },
    '*': { pattern: /\d|\*|×/, regexpToFind: /\*/g },
    '$': { pattern: /\w/, regexpToFind: /\$/g },
    '@': { pattern: /7/ },
    '%': { pattern: /8/ },
    'H': { pattern: /[\dа-яёА-ЯЁ]/ },
    'Z': { pattern: /[\dа-яА-Яa-zA-Z]/ },
    'E': { pattern: /[a-zA-Z\[\];'`\{\}:"<>~,\.\-\s]+|[а-яА-Я\-\sёЁ]+/ },
    'D': { pattern: /[a-zA-ZА-Яа-я\\-\\-\\sёЁ ]/ },
    'L': { pattern: /\w|[@\.\-]/ }, // pattern for login/phone field (numbers, letters, @_.-)
    'U': { pattern: /[авекмнорстухabekmhopctyx\d]/i } // pattern for drivers license
};
/* eslint-enable */

const mask = (value, mask) => { // eslint-disable-line max-statements
    let maskedValue = '',
        isPure = false,
        unmaskedValue = '',
        maskLen = mask.length,
        valueLen = (typeof (value) === 'undefined') ? 0 : value.length,
        maskStep = 0,
        valueStep = 0,
        nextSpecial = false,
        currentTransition,
        currentValueChar,
        currentMaskChar;

    while (maskStep < maskLen && valueStep < maskLen) {
        currentValueChar = valueStep < valueLen && value[valueStep] || '';
        currentMaskChar = mask[maskStep];

        if (currentMaskChar === '\\' && !nextSpecial) {
            maskStep++;
            nextSpecial = true;
            continue;
        }

        currentTransition = nextSpecial ?
            null :
            maskTransitions[currentMaskChar];

        nextSpecial = false;

        if (currentTransition) {
            if (currentValueChar.match(currentTransition.pattern)) {
                maskedValue += currentValueChar;
                unmaskedValue += currentValueChar;
                maskStep++;
                valueStep++;
            } else {
                valueStep++;
            }
        } else {
            maskedValue += currentMaskChar;
            maskStep++;

            if (currentValueChar === currentMaskChar && !isPure) {
                valueStep++;
            } else {
                isPure = true;
            }
        }
    }

    return {
        maskedValue: maskedValue.slice(0, maskedValue.length),
        unmaskedValue,
        maskStep,
        valueStep
    };
};

export {
    mask
};
