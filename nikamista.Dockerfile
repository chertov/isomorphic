FROM node:alpine
MAINTAINER Chertov Maxim <chertovmv@gmail.com>

ENV KEYMETRICS_PUBLIC=h7oax5qowb8d4qs
ENV KEYMETRICS_SECRET=5d27eatic0l014c

RUN set -x \
  && apk add --no-cache --virtual .run-deps \
    ca-certificates

RUN npm install -g pm2
RUN pm2 install pm2-server-monit

COPY ./bin/ /app/bin/
COPY ./pm2_process.yml /app/bin/
COPY ./pub/ /app/pub/

WORKDIR /app/bin/
# CMD ["pm2-docker", "/app/bin/pm2_process.yml"]
ENTRYPOINT pm2-docker /app/bin/pm2_process.yml
