
    // Точка входа для рендера на стороне клиента

    import React from 'react';
    import ReactDOM from 'react-dom';
    import { Provider } from 'react-redux';
    import ApolloClient, { createNetworkInterface } from 'apollo-client';

    import ClientProvider from 'clientprovider.jsx';
    import { validateOrder } from 'api/order';
    import getStore from './store';
    import router from './router.jsx';
    import { exists, onserver, getHash, graphqlHost } from './tools';

    const client = new ApolloClient({
        ssrMode: false,
        queryDeduplication: false,
        // addTypename: false,
        // Remember that this is the interface the SSR server will use to connect to the
        // API server, so we need to ensure it isn't firewalled, etc
        // connectToDevTools: true,
        networkInterface: createNetworkInterface({
            uri: `${graphqlHost()}query`,
            opts: {
                credentials: 'same-origin',
                headers: {
                    'Cache-Control': 'no-cache'
                }
            }
        })
    });

    /* global document */
    document.addEventListener('DOMContentLoaded', event => {
        // - Code to execute when all DOM content is loaded.
        // - including fonts, images, etc.
        console.log('On server rendering: ', onserver());

        fetch('/tree.json')
        .then(response => response.json())
        .then(tree => {
            if (exists(treeHash)) {
                const loadedTreeHash = getHash(JSON.stringify(tree));

                if (loadedTreeHash !== treeHash) {
                    console.log('reload without cache');
                    const headers = new Headers();

                    headers.append('pragma', 'no-cache');
                    headers.append('cache-control', 'no-cache');

                    return fetch(new Request('/tree.json'), { method: 'GET', headers })
                    .then(response => response.json());
                }
            }
            return tree;
        })
        .then(tree => {
            const store = getStore({ tree });

            validateOrder(store);
            console.log('state', store.getState());
            ReactDOM.render(
                <Provider store={store}>
                    <ClientProvider client={client}>
                        {router}
                    </ClientProvider>
                </Provider>,
                document.getElementById('root')
            );
        });
    });
