
    export function exists(variable) {
        return (typeof variable !== 'undefined' && variable !== null);
    }

    /* global window */

    export function onserver() {
        return !(typeof window !== 'undefined' && window !== null);
    }

    export function graphqlHost() {
        if (!onserver()) {
            // формируем адрес для запросов к GraphQL
            return `${location.protocol}//${location.hostname}${location.port ? `:${location.port}` : ''}/`;
        }
        return 'http://127.0.0.1:8080/';
    }

    const meta = {};

    export function getMeta(title) {
        return meta;
    }

    export function setHtmlTitle(title) {
        const newTitle = title;

        if (onserver()) {
            meta.title = newTitle;
            return;
        }
        if (document.title !== newTitle) {
            document.title = newTitle;
        }
    }

    export function logerr(error) {
        if (onserver()) {
            console.error(error);
            return;
        }
        console.error(error);
    }

    export function parseApolloError(error, index = null) {
        if (!error.graphQLErrors) { return { code: 0, enum: 'FRONTEND_ERROR', msg: error }; }
        try {
            const errors = error.graphQLErrors.map(gqlerror => JSON.parse(gqlerror.message));

            if (index !== null) {
                if (index >= 0 && index < errors.length) {
                    return errors[index];
                }
            }
            return errors;
        } catch (jsonerr) {
            return { code: 0, enum: 'FRONTEND_ERROR', msg: error };
        }
    }

    export function findId(tree, id) {
        let entity = null;

        entity = tree.folders[id];
        if (entity) { return entity; }
        entity = tree.categories[id];
        if (entity) { return entity; }
        entity = tree.products[id];
        if (entity) { return entity; }
        entity = tree.models[id];
        if (entity) { return entity; }
        return null;
    }
    export function getClearUrlPath(tree, id) {
        const node = findId(tree, id);

        if (!node) { return ''; }

        return `${getClearUrlPath(tree, node.parent)}/${id}`;
    }
    export function getUrlPath(tree, id) {
        const clearUrl = getClearUrlPath(tree, id);

        if (clearUrl) {
            return `/catalog${clearUrl}`;
        }
        return null;
    }

    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     * param  iNumber Integer Число на основе которого нужно сформировать окончание
     * param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
     *         например ['яблоко', 'яблока', 'яблок']
     * return String
     */
    export function getPlural(iNumber, aEndings) {
        let sEnding;
        let i;

        i = iNumber % 100;
        if (i >= 11 && i <= 19) {
            sEnding = aEndings[2];
        } else {
            i %= 10;
            switch (i) {
                case (1): sEnding = aEndings[0]; break;
                case (2):
                case (3):
                case (4): sEnding = aEndings[1]; break;
                default: sEnding = aEndings[2];
            }
        }
        return sEnding;
    }

    /* eslint-disable */
    const murmurhash3_32_gc = (key, seed) => {
        var remainder,
            bytes,
            h1,
            h1b,
            c1,
            c1b,
            c2,
            c2b,
            k1,
            i;

        remainder = key.length & 3; // key.length % 4
        bytes = key.length - remainder;
        h1 = seed;
        c1 = 0xcc9e2d51;
        c2 = 0x1b873593;
        i = 0;

        while (i < bytes) {
          	k1 =
                ((key.charCodeAt(i) & 0xff)) |
                ((key.charCodeAt(++i) & 0xff) << 8) |
                ((key.charCodeAt(++i) & 0xff) << 16) |
                ((key.charCodeAt(++i) & 0xff) << 24);
            ++i;

            k1 = ((((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16))) & 0xffffffff;
            k1 = (k1 << 15) | (k1 >>> 17);
            k1 = ((((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16))) & 0xffffffff;

            h1 ^= k1;
            h1 = (h1 << 13) | (h1 >>> 19);
            h1b = ((((h1 & 0xffff) * 5) + ((((h1 >>> 16) * 5) & 0xffff) << 16))) & 0xffffffff;
            h1 = (((h1b & 0xffff) + 0x6b64) + ((((h1b >>> 16) + 0xe654) & 0xffff) << 16));
        }

        k1 = 0;

        switch (remainder) {
            case 3: k1 ^= (key.charCodeAt(i + 2) & 0xff) << 16;
            case 2: k1 ^= (key.charCodeAt(i + 1) & 0xff) << 8;
            case 1: k1 ^= (key.charCodeAt(i) & 0xff);

                k1 = (((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16)) & 0xffffffff;
                k1 = (k1 << 15) | (k1 >>> 17);
                k1 = (((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16)) & 0xffffffff;
                h1 ^= k1;
        }

        h1 ^= key.length;

        h1 ^= h1 >>> 16;
        h1 = (((h1 & 0xffff) * 0x85ebca6b) + ((((h1 >>> 16) * 0x85ebca6b) & 0xffff) << 16)) & 0xffffffff;
        h1 ^= h1 >>> 13;
        h1 = ((((h1 & 0xffff) * 0xc2b2ae35) + ((((h1 >>> 16) * 0xc2b2ae35) & 0xffff) << 16))) & 0xffffffff;
        h1 ^= h1 >>> 16;

        return h1 >>> 0;
    };
    /* eslint-enable */

    export const getHash = str => murmurhash3_32_gc(str, 0);
