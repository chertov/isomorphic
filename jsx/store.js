
import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { onserver, exists } from './tools';
import reducers from './reducers';
import rootSaga from './sagas';

/* global window */

let getStore = null;

if (onserver()) {
    getStore = (initState = null) => {
        let stateValue = {};
        if(initState === null) { return null; }
        stateValue = Object.assign(stateValue, initState);

        const sagaMiddleware = createSagaMiddleware();
        let composes = compose(applyMiddleware(sagaMiddleware));
        const store = createStore( reducers, stateValue, composes );

        sagaMiddleware.run(rootSaga);
        return store;
    };
} else {
    let store = null;

    getStore = (initState = null) => {
        if(store !== null) { return store; }

        let stateValue = {};
        if (exists(windowStateValue)) {
            stateValue = Object.assign(stateValue, windowStateValue)
        }
        stateValue = Object.assign(stateValue, initState);

        const sagaMiddleware = createSagaMiddleware();
        let composes = compose(
            applyMiddleware(sagaMiddleware),
            // If you are using the devToolsExtension, you can add it here also
            window.devToolsExtension ? window.devToolsExtension() : f => f,
        );

        store = createStore( reducers, stateValue, composes );
        sagaMiddleware.run(rootSaga);
        return store;
    };
}

export default getStore;
