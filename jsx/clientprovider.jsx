
    import React, { Component, PropTypes as pt } from 'react';

    class ClientProvider extends Component {
        static propTypes = {
            client: pt.object
        }
        static childContextTypes = {
            client: pt.object
        }
        getChildContext() {
            return {
                client: this.props.client
            };
        }
        render() {
            return this.props.children;
        }
    }

    export default ClientProvider;
