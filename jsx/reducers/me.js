
    export const LOGIN_LOADER = 'LOGIN_LOADER';

    export const GET_ME = 'GET_ME';
    export const LOGIN_ME = 'LOGIN_ME';
    export const LOGOUT_ME = 'LOGOUT_ME';
    export const NEWPASSWORD_ME = 'NEWPASSWORD_ME';
    export const RELOAD_ALL = 'RELOAD_ALL';

    export const REQUEST_ME = 'REQUEST_ME';
    export const RECEIVE_ME = 'RECEIVE_ME';
    export const RECEIVE_ERROR_ME = 'RECEIVE_ERROR_ME';

    export default (state = null, action) => {
        switch (action.type) {
            case REQUEST_ME:
                return state;
            case RECEIVE_ME:
                console.log('new me', action.me);
                return action.me;
            case RECEIVE_ERROR_ME:
                // TODO: обработать ошибку
                console.log('error', action.error);
                return state;
            default:
                return state;
        }
    };
