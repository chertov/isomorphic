
    export const SET_ERROR = 'SET_ERROR';
    export const CLEAR_ERROR = 'CLEAR_ERROR';

    export default (state = {}, action) => {
        switch (action.type) {
            case SET_ERROR: {
                const newState = Object.assign({}, state);

                newState[action.id] = action.error;
                return newState;
            }
            case CLEAR_ERROR: {
                const newState = Object.assign({}, state);

                delete newState[action.id];
                return newState;
            }
            default:
                return state;
        }
    };
