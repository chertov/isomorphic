
    export const SHOW_ERROR = 'SHOW_ERROR';
    export const HIDE_ERROR = 'HIDE_ERROR';

    export default (state = { show: false, text: '' }, action) => {
        switch (action.type) {
            case SHOW_ERROR:
                return {
                    show: true,
                    text: action.text
                };
            case HIDE_ERROR:
                return {
                    show: false,
                    text: ''
                };
            default:
                return state;
        }
    };
