
    import { combineReducers } from 'redux';
    import me from './me';
    import tree from './tree';
    import products from './products';
    import error from './error';
    import errors from './errors';
    import order from './order';
    import activeLoaders from './activeLoaders';

    // объединяем в кучу
    export default combineReducers({
        me,
        order,
        tree,
        products,
        error,
        errors,
        activeLoaders
    });
