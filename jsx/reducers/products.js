
    export const PRODUCTS_LOADER = 'PRODUCTS_LOADER';

    export const GET_PRODUCTS = 'GET_PRODUCTS';

    export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';
    export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';
    export const RECEIVE_ERROR_PRODUCTS = 'RECEIVE_ERROR_PRODUCTS';

    export default (state = {}, action) => {
        const newState = Object.assign({}, state);

        switch (action.type) {
            case REQUEST_PRODUCTS:
                return newState;
            case RECEIVE_PRODUCTS:
                return action.products;
            case RECEIVE_ERROR_PRODUCTS:
                // TODO: обработать ошибку
                console.log('error', action.error);
                return newState;
            default:
                return newState;
        }
    };
