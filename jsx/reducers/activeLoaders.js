
    export const SHOW_LOADER = 'SHOW_LOADER';
    export const HIDE_LOADER = 'HIDE_LOADER';

    export default (state = {}, action) => {
        switch (action.type) {
            case SHOW_LOADER: {
                const newState = Object.assign({}, state);

                newState[action.id] = true;
                return newState;
            }
            case HIDE_LOADER: {
                const newState = Object.assign({}, state);

                delete newState[action.id];
                return newState;
            }
            default:
                return state;
        }
    };
