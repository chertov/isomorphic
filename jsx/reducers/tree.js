
    export const TREE_LOADER = 'TREE_LOADER';

    export const GET_TREE = 'GET_TREE';

    export const REQUEST_TREE = 'REQUEST_TREE';
    export const RECEIVE_TREE = 'RECEIVE_TREE';
    export const RECEIVE_ERROR_TREE = 'RECEIVE_ERROR_TREE';

    export default (state = {}, action) => {
        const newState = Object.assign({}, state);

        switch (action.type) {
            case REQUEST_TREE:
                return newState;
            case RECEIVE_TREE:
                return action.tree;
            case RECEIVE_ERROR_TREE:
                // TODO: обработать ошибку
                console.log('error', action.error);
                return newState;
            default:
                return newState;
        }
    };
