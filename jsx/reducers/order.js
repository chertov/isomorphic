
    export const ORDER_LOADER = 'ORDER_LOADER';

    export const REQUEST_ORDER = 'REQUEST_ORDER';
    export const RECEIVE_ORDER = 'RECEIVE_ORDER';

    export const GET_ORDER = 'GET_ORDER';
    export const DEFAULT_ORDER = 'DEFAULT_ORDER';
    export const CHANGE_ORDER = 'CHANGE_ORDER';
    export const VALIDATE_ORDER = 'VALIDATE_ORDER';
    export const UPDATE_ORDER = 'UPDATE_ORDER';
    export const MAKE_ORDER = 'MAKE_ORDER';

    // export const order = (state = { phone: 7, name: 'Client1', address: 'Addsress', items: [] }, action) => {
    //     const newState = Object.assign({}, state);
    //
    //     switch (action.type) {
    //         case CHANGE_ORDER:
    //             const order = Object.assign({}, action.order);
    //
    //             order.phone = parseFloat(parseInt(order.phone, 10));
    //             return order;
    //         default:
    //             return newState;
    //     }
    // };

    export const orderDefault = {
        name: '',
        familyName: '',
        email: '',
        phone: '',
        comment: '',

        isCompany: false,
        company: {
            name: '',
            phone: '',
            jurAddress: '',
            factAddress: '',
            inn: '',
            kpp: '',
            okpo: '',
            korrSchet: '',
            raschSchet: '',
            bik: '',
            bank: ''
        },
        fizik: {
            passport: ''
        },
        delivery: {
            type: '',
            address: ''
        },
        items: []
    };

    export default (state = orderDefault, action) => {
        const newState = Object.assign({}, state);

        switch (action.type) {
            case REQUEST_ORDER:
                return newState;
            case DEFAULT_ORDER:
                return Object.assign({}, orderDefault);
            case RECEIVE_ORDER:
                console.log('RECEIVE_ORDER...');
                return Object.assign({}, action.order);
            default:
                return newState;
        }
    };
