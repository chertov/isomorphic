import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import React, { PropTypes } from 'react';

import API from 'api/Api.jsx';
import './components/common/common.less';

import Layout from './components/layouts/Layout.jsx';
import Main from './components/pages/main/Main.jsx';
import About from './components/pages/about/About.jsx';
import Delivery from './components/pages/delivery/Delivery.jsx';
import Catalog from './components/pages/catalog/Catalog.jsx';
import Order from './components/pages/order/Order.jsx';
import OrderSuccess from './components/pages/ordersuccess/OrderSuccess.jsx';
import Orders from './components/pages/orders/Orders.jsx';
import Onlays from './components/pages/onlays/Onlays.jsx';
import Product from './components/pages/product/Product.jsx';
import Armchairs from './components/pages/armchairs/Armchairs.jsx';
import Massage from './components/pages/massage/Massage.jsx';

import Profile from './components/pages/profile/Profile.jsx';
import Page404 from './components/pages/page404/Page404.jsx';

function handleUpdate() {
    const { hash } = window.location;
    const { action, state } = this.state.location;

    if (state && state.noScroll) {
        return;
    }

    if (hash === '' && action === 'PUSH') {
        window.scrollTo(0, 0);
    }

    if (hash !== '') {
        setTimeout(() => {
            const id = hash.replace('#', '');
            const element = document.getElementById(id);

            if (element) {
                element.scrollIntoView();
            }
        }, 0);
    }
}

const App = ({ children }) =>
    <Layout>
        {children}
    </Layout>;

App.propTypes = { children: PropTypes.element.isRequired };

export const Routes = {
    path: '/',
    paths: [
        { component: API, path: '**/(:modelId)/add' },
        { component: API, path: '**/(:modelId)/rm' },
        { component: API, path: '**/(:modelId)/inc' },
        { component: API, path: '**/(:modelId)/dec' },
        { component: API, path: '**/(:modelId)/decrm' },

        { component: Profile, path: 'profile' },

        { component: About, path: 'about' },
        { component: Delivery, path: 'delivery' },
        { component: Order, path: 'order' },
        { component: Orders, path: 'orders' },
        { component: Onlays, path: 'onlays' },
        { component: Product, path: 'product' },
        { component: Armchairs, path: 'armchairs' },
        { component: Massage, path: 'massage' },
        { component: Catalog, path: 'catalog' },
        { component: Catalog, path: 'catalog/(:c0)(/:c1)(/:c2)(/:c3)(/:c4)(/:c5)(/:c6)(/:c7)(/:c8)(/:c9)(/:c10)(/:c11)(/:c12)(/:c13)(/:c14)(/:c15)(/:c16)(/:c17)(/:c18)(/:c19)' }
        // { component: Catalog, path: '(:c0)(/:c1)(/:c2)(/:c3)(/:c4)(/:c5)(/:c6)(/:c7)(/:c8)(/:c9)(/:c10)(/:c11)(/:c12)(/:c13)(/:c14)(/:c15)(/:c16)(/:c17)(/:c18)(/:c19)' },
    ],
    catalogPaths: [],
    nomatch: { component: Page404, path: '*' },

    getPaths: () => Routes.paths.concat(Routes.catalogPaths).concat([Routes.nomatch]),

    getChildRoutes: (partialNextState, callback) => callback(null, Routes.getPaths()),
    getIndexRoute: (partialNextState, callback) => callback(null, { component: Main }),
    getComponents: (nextState, callback) => callback(null, App)
};

// const treeDetect = setInterval(() => {
//     const store = getStore();
//
//     if (store && store.getState().tree.folders) {
//         clearInterval(treeDetect);
//         setTimeout(() => {
//             const tree = store.getState().tree;
//
//             console.log('treeDetect', tree);
//
//             const path = [];
//             const paths = [];
//
//             const save_tree = node => {
//                 if (node.folders) {
//                     node.folders.map((folder, i) => {
//                         path.push(folder.id);
//                         paths.push(`/catalog/${path.join('/')}`);
//                         // console.log('folder', path.join(' / '));
//                         save_tree(folder);
//                         path.pop();
//                     });
//                 }
//                 if (node.categories) {
//                     node.categories.map((category, i) => {
//                         path.push(category.id);
//                         paths.push(`/catalog/${path.join('/')}`);
//                         // console.log('category', path.join(' / '));
//                         save_tree(category);
//                         path.pop();
//                     });
//                 }
//                 if (node.items) {
//                     node.items.map((item, i) => {
//                         path.push(item.id);
//                         // paths.push(`/catalog/${path.join('/')}`);
//                         // console.log('item', path.join(' / '));
//                         save_tree(item);
//                         path.pop();
//                     });
//                 }
//             };
//
//             save_tree(tree);
//             console.log(paths.length);
//             paths.map((path, i) => {
//                 console.log('path', path);
//             });
//
//             Routes.catalogPaths.unshift({
//                 component: Catalog,
//                 path: 'catalog/(:c0)(/:c1)(/:c2)(/:c3)(/:c4)(/:c5)(/:c6)(/:c7)(/:c8)(/:c9)(/:c10)(/:c11)(/:c12)(/:c13)(/:c14)(/:c15)(/:c16)(/:c17)(/:c18)(/:c19)'
//             });
//         }, 5000);
//     }
// }, 1000);

// export default
//     <Router
//         history={browserHistory}
//         onUpdate={handleUpdate}
//     >
//         <Route
//             path={Routes.path}
//             getChildRoutes={Routes.getChildRoutes}
//             getIndexRoute={Routes.getIndexRoute}
//             getComponents={Routes.getComponents}
//         />
//     </Router>;

export default
    <Router
        history={browserHistory}
        onUpdate={handleUpdate}
    >
        <Route path='**/(:modelId)/add' component={API} />
        <Route path='**/(:modelId)/rm' component={API} />
        <Route path='**/(:modelId)/inc' component={API} />
        <Route path='**/(:modelId)/dec' component={API} />
        <Route path='**/(:modelId)/decrm' component={API} />

        <Route path='/' component={App}>
            <IndexRoute component={Main} />
            <Route path='profile' component={Profile}>
                <IndexRoute component={Orders} />
                <Route path='orders' component={Orders} />
            </Route>
            <Route path='about' component={About} />
            <Route path='delivery' component={Delivery} />
            <Route path='order' component={Order} />
            <Route path='ordersuccess/:ordernumber' component={OrderSuccess} />
            <Route path='onlays' component={Onlays} />
            <Route path='product' component={Product} />
            <Route path='armchairs' component={Armchairs} />
            <Route path='massage' component={Massage} />
            <Route path='catalog' component={Catalog} />
            <Route path='catalog/(:c0)(/:c1)(/:c2)(/:c3)(/:c4)(/:c5)(/:c6)(/:c7)(/:c8)(/:c9)(/:c10)(/:c11)(/:c12)(/:c13)(/:c14)(/:c15)(/:c16)(/:c17)(/:c18)(/:c19)' component={Catalog} />

            <Route path='*' component={Page404} />
        </Route>
    </Router>;
