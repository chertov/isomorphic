
    import { put, call, takeEvery } from 'redux-saga/effects';

    import { RELOAD_ALL, RECEIVE_ME } from 'reducers/me';
    import { RECEIVE_ORDER } from 'reducers/order';
    import { getMeQuery } from 'sagas/me';
    import { getOrderQuery } from 'sagas/order/get';

    function* reloadAll(action) {
        try {
            console.log('reloader');
            const me = yield call(getMeQuery, action.client);
            const order = yield call(getOrderQuery, action.client);

            yield put({ type: RECEIVE_ME, me });
            console.log('reloadall order', order);
            yield put({ type: RECEIVE_ORDER, order });
        } catch (error) {
        }
    }

    export default function* rootSaga() {
        yield takeEvery(RELOAD_ALL, reloadAll);
    }
