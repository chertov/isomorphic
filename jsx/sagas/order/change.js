
    import { call, put, takeEvery } from 'redux-saga/effects';
    import { updateOrder, validateOrder } from 'api/order';
    import { CHANGE_ORDER, RECEIVE_ORDER } from 'reducers/order';

    let timeoutUpdate;

    function* changeOrder(action) {
        try {
            yield put({ type: RECEIVE_ORDER, order: action.order });

            clearTimeout(timeoutUpdate);
            timeoutUpdate = setTimeout(action => {
                updateOrder(action.store.getState().order, action.store, action.client);
            }, 1000, action);

            yield call(validateOrder, action.store);
        } catch (error) { }
    }

    export default function* rootSaga() {
        yield takeEvery(CHANGE_ORDER, changeOrder);
    }
