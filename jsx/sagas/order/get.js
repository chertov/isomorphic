
    import { gql, orderFragment } from 'gql';

    export const getOrderQuery = client =>
        client.query({
            query: gql`
                query {
                    GetOrder {
                        ...orderFragment
                    }
                }
                ${orderFragment}
            `,
            fetchPolicy: 'network-only'
        }).then(response => response.data.GetOrder);
