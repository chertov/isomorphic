
    import { fork } from 'redux-saga/effects';
    import change from './change';
    import validate from './validate';
    import update from './update';
    import make from './make';

    export default function* rootSaga() {
        yield [
            fork(update),
            fork(validate),
            fork(change),
            fork(make)
        ];
    }
