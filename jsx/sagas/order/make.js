
    import { call, takeEvery } from 'redux-saga/effects';

    import { showLoader, hideLoader } from 'api/activeLoaders';
    import { setError, clearError } from 'api/error';
    import { ERROR_ORDER } from 'api/order';
    import { MAKE_ORDER, ORDER_LOADER } from 'reducers/order';
    import { gql, orderFragment } from 'gql';
    import { parseApolloError, logerr } from 'tools';

    const makeOrderQuery = (Order, client) =>
        client.mutate({
            mutation: gql`
                mutation {
                    MakeOrder(Order: $Order) {
                        ...orderFragment
                        confimCode
                    }
                }
                ${orderFragment}
            `,
            variables: { Order }
        }).then(response => response.data.MakeOrder)
        .catch(error => { throw parseApolloError(error, 0); });

    function* makeOrder(action) {
        try {
            yield call(showLoader, ORDER_LOADER, action.store);
            yield call(clearError, ERROR_ORDER, action.store);

            const order = yield call(makeOrderQuery, action.order, action.client);

            yield call(action.callback, order);

            // yield put({ type: RELOAD_ALL, client: action.client });
            // yield call(browserHistory.push, { pathname: '/ordersuccess', state: { noScroll: false } });
        } catch (error) {
            if (Array.isArray(error)) {
                yield call(setError, action.errorId, error, action.store);
            } else {
                if (error.name) {
                    yield call(setError, action.errorId, [error], action.store);
                } else {
                    logerr(error);
                }
            }
        }
        yield call(hideLoader, ORDER_LOADER, action.store);
    }

    export default function* rootSaga() {
        yield takeEvery(MAKE_ORDER, makeOrder);
    }
