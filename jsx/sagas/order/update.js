
    import { call, takeEvery } from 'redux-saga/effects';

    import { UPDATE_ORDER } from 'reducers/order';
    import { gql, orderFragment } from 'gql';
    import { parseApolloError, logerr } from 'tools';

    const updateOrderQuery = (Order, client) =>
        client.mutate({
            mutation: gql`
                mutation {
                    UpdateOrder(Order: $Order) {
                        ...orderFragment
                    }
                }
                ${orderFragment}
            `,
            variables: { Order }
        }).then(response => response.data.UpdateOrder)
        .catch(error => { throw parseApolloError(error, 0); });

    function* updateOrder(action) {
        try {
            yield call(updateOrderQuery, action.order, action.client);
            console.log('Update order...');
        } catch (error) { logerr(error); }
    }

    export default function* rootSaga() {
        yield takeEvery(UPDATE_ORDER, updateOrder);
    }
