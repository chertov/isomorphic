
    import { call, takeEvery } from 'redux-saga/effects';
    import { ERROR_ORDER } from 'api/order';
    import { setError } from 'api/error';
    import { VALIDATE_ORDER } from 'reducers/order';

    const phoneCheck = /^\+7[0-9]{10}$/;
    const innCheck = /^([0-9]{10}|[0-9]{12})$/;
    const kppCheck = /^[0-9]{9}$/;
    const okpoCheck = /^([0-9]{8}|[0-9]{10})$/;
    const korrCheck = /^301[0-9]{17}$/;
    const raschCheck = /^[0-9]{20}$/;
    const bikCheck = /^04[0-9]{7}$/;
    const check = (checker, value) => value === '' ? true : checker.test(value);
    const orderValidate = order => {
        const errors = [];

        if (!check(phoneCheck, order.phone)) { errors.push({ name: 'PHONE_ISNT_VALID' }); }
        if (order.isCompany) {
            if (!check(innCheck, order.company.inn)) { errors.push({ name: 'INN_ISNT_VALID' }); }
            if (!check(kppCheck, order.company.kpp)) { errors.push({ name: 'KPP_ISNT_VALID' }); }
            if (!check(okpoCheck, order.company.okpo)) { errors.push({ name: 'OKPO_ISNT_VALID' }); }
            if (!check(korrCheck, order.company.korrSchet)) { errors.push({ name: 'KORR_ISNT_VALID' }); }
            if (!check(raschCheck, order.company.raschSchet)) { errors.push({ name: 'RASCH_ISNT_VALID' }); }
            if (!check(bikCheck, order.company.bik)) { errors.push({ name: 'BIK_ISNT_VALID' }); }
        }
        return errors;
    };

    function* validateOrderGen(action) {
        const errors = orderValidate(action.store.getState().order);

        yield call(setError, ERROR_ORDER, errors, action.store);
        yield call(action.callback);
    }

    export default function* rootSaga() {
        yield takeEvery(VALIDATE_ORDER, validateOrderGen);
    }
