
    import { put, call, takeEvery } from 'redux-saga/effects';

    import { showLoader, hideLoader } from 'api/activeLoaders';
    import { gql, modelFragment, productFragment, categoryFragment, folderFragment } from '../../gql';
    import { GET_TREE, TREE_LOADER, REQUEST_TREE, RECEIVE_TREE, RECEIVE_ERROR_TREE } from '../../reducers/tree';

    export const getTreeQuery = client =>
        client.query({
            query: gql`
                query {
                    rootfolders
                    rootcategories
                    folders {
                        ...folderFragment
                    }
                    categories {
                        ...categoryFragment
                    }
                    products {
                        ...productFragment
                    }
                    models {
                        ...modelFragment
                    }
                }
                ${folderFragment}
                ${categoryFragment}
                ${productFragment}
                ${modelFragment}
            `
        }).then(response => response.data);

    function* getTree(action) {
        try {
            yield call(() => showLoader(TREE_LOADER));
            yield put({ type: REQUEST_TREE });
            const tree = yield call(getTreeQuery, action.client);

            yield put({ type: RECEIVE_TREE, tree });
            yield call(() => hideLoader(TREE_LOADER));
        } catch (error) {
            yield call(() => hideLoader(TREE_LOADER));
            yield put({ type: RECEIVE_ERROR_TREE, error });
        }
    }

    export default function* rootSaga() {
        yield takeEvery(GET_TREE, getTree);
    }
