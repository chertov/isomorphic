
    import { fork } from 'redux-saga/effects';
    import gettree from './gettree';

    export default function* rootSaga() {
        yield [
            fork(gettree)
        ];
    }
