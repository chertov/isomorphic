
    import { put, call, takeEvery } from 'redux-saga/effects';
    import { delay } from 'redux-saga';

    import { LOGIN_LOADER, LOGIN_ME, LOGOUT_ME, NEWPASSWORD_ME, RECEIVE_ME, RECEIVE_ERROR_ME } from 'reducers/me';
    import { DEFAULT_ORDER, RECEIVE_ORDER } from 'reducers/order';
    import { getMeQuery } from 'sagas/me';
    import { showLoader, hideLoader } from 'api/activeLoaders';
    import { setError, clearError } from 'api/error';
    import { getOrderQuery } from 'sagas/order/get';
    import { gql, userFragment, orderFragment } from 'gql';
    import { parseApolloError, logerr } from 'tools';

    const loginQuery = (phone = '', password = '', client) =>
        client.mutate({
            mutation: gql`
                mutation {
                    Login(phone: $phone, password: $password) {
                        ...userFragment
                    }
                }
                ${userFragment}
                ${orderFragment}
            `,
            fetchPolicy: 'network-only',
            variables: { phone, password }
        }).then(response => response.data.Login)
        .catch(error => { throw parseApolloError(error, 0); });

    function* loginMe(action) {
        try {
            yield call(showLoader, LOGIN_LOADER, action.store);
            yield call(clearError, action.errorId, action.store);
            // yield call(delay, 2000, true);

            yield call(loginQuery, action.phone, action.password, action.client);
            const me = yield call(getMeQuery, action.client);
            const order = yield call(getOrderQuery, action.client);

            yield put({ type: RECEIVE_ME, me });
            yield put({ type: RECEIVE_ORDER, order });
            yield call(action.callback);
        } catch (error) {
            if (Array.isArray(error)) {
                yield call(setError, action.errorId, error, action.store);
            } else {
                if (error.name) {
                    yield call(setError, action.errorId, [error], action.store);
                } else {
                    logerr(error);
                }
            }
        }
        yield call(hideLoader, LOGIN_LOADER, action.store);
    }

    const logoutQuery = client =>
        client.mutate({
            mutation: gql`
                mutation {
                    Logout {
                        ...userFragment
                    }
                }
                ${userFragment}
                ${orderFragment}
            `,
            fetchPolicy: 'network-only'
        }).then(response => response.data.Logout)
        .catch(error => { throw parseApolloError(error, 0); });

    function* logoutMe(action) {
        try {
            yield call(showLoader, LOGIN_LOADER, action.store);
            yield call(clearError, action.errorId, action.store);
            yield call(logoutQuery, action.client);
            yield put({ type: RECEIVE_ME, me: null });
            yield put({ type: DEFAULT_ORDER });
            yield call(action.callback);
        } catch (error) {
            console.log('logout error', error);
            if (Array.isArray(error)) {
                yield call(setError, action.errorId, error, action.store);
            } else {
                if (error.name) {
                    yield call(setError, action.errorId, [error], action.store);
                } else {
                    logerr(error);
                }
            }
        }
        yield call(hideLoader, LOGIN_LOADER, action.store);
    }

    const newPasswordQuery = (phone = '', client) =>
        client.mutate({
            mutation: gql`
                mutation {
                    NewPassword(phone: $phone) {
                        string
                    }
                }
            `,
            fetchPolicy: 'network-only',
            variables: { phone }
        }).then(response => response.data.NewPassword)
        .catch(error => { throw parseApolloError(error, 0); });

    function* newPasswordMe(action) {
        try {
            yield call(showLoader, LOGIN_LOADER, action.store);
            yield call(clearError, action.errorId, action.store);
            yield call(newPasswordQuery, action.phone, action.client);
            yield call(action.callback);
        } catch (error) {
            if (Array.isArray(error)) {
                yield call(setError, action.errorId, error, action.store);
            } else {
                if (error.name) {
                    yield call(setError, action.errorId, [error], action.store);
                } else {
                    logerr(error);
                }
            }
        }
        yield call(hideLoader, LOGIN_LOADER, action.store);
    }

    export default function* rootSaga() {
        yield takeEvery(LOGIN_ME, loginMe);
        yield takeEvery(LOGOUT_ME, logoutMe);
        yield takeEvery(NEWPASSWORD_ME, newPasswordMe);
    }
