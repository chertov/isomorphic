
    import { put, call, takeEvery } from 'redux-saga/effects';

    import { GET_ME, REQUEST_ME, RECEIVE_ME, RECEIVE_ERROR_ME } from 'reducers/me';
    import { gql, userFragment, orderFragment } from 'gql';
    import { parseApolloError } from 'tools';

    export const getMeQuery = client =>
        client.query({
            query: gql`
                query {
                    me {
                        ...userFragment
                    }
                }
                ${userFragment}
                ${orderFragment}
            `,
            fetchPolicy: 'network-only'
        }).then(response => response.data.me)
        .catch(error => { throw parseApolloError(error)[0]; });

    function* getMe(action) {
        try {
            yield put({ type: REQUEST_ME });
            const me = yield call(getMeQuery, action.client);

            console.log('me', me);
            yield put({ type: RECEIVE_ME, me });
        } catch (error) {
            yield put({ type: RECEIVE_ERROR_ME, error });
        }
    }

    export default function* rootSaga() {
        yield takeEvery(GET_ME, getMe);
    }
