
    import gqlTag from 'graphql-tag';

    export const gql = gqlTag;

    export const userFragment = `
        fragment userFragment on User {
            name
            familyName
            email
            phone

            ordershistory {
                ... orderFragment
            }
        }
    `;

    export const cartFragment = `
        fragment cartFragment on CartItem {
            ModelId
            Count
        }
    `;

    export const modelFragment = `
        fragment modelFragment on Model {
            id
            parent
            product
            title
            shortTitle
            summary
            description
            images
            price {
                currency
                value
            }
            properties {
                id
                value
                values
            }
        }
    `;

    export const productFragment = `
        fragment productFragment on Product {
            id
            parent
            title
            shortTitle
            summary
            description
            images
            models
        }
    `;

    export const categoryFragment = `
        fragment categoryFragment on Category {
            id
            parent
            title
            shortTitle
            summary
            description
            image
            properties {
                id
                type
                title
                enum {
                    type
                    values {
                        id
                        value
                    }
                }
            }
            products
        }
    `;

    export const folderFragment = `
        fragment folderFragment on Folder {
            id
            parent
            title
            shortTitle
            summary
            description
            image
            folders
            categories
        }
    `;

    export const orderFragment = `
        fragment orderFragment on Order {
            id
            number

            name
            surname
            patronymic
            email
            phone
            comment

            isCompany
            company {
                name
                phone
                jurAddress
                factAddress
                inn
                kpp
                okpo
                korrSchet
                raschSchet
                bik
                bank
            }
            fizik {
                passport
            }
            delivery {
                type
                address
            }

            state
            messages {
                time
                message
            }
            items {
                modelId
                count
                article
                code
                pTitle
                title
                price {
                    currency
                    value
                }
            }
        }
    `;
