
    import { SHOW_ERROR, HIDE_ERROR } from 'reducers/error';
    import { SET_ERROR, CLEAR_ERROR } from 'reducers/errors';
    import getStore from './../store';

    export const showError = text => {
        getStore().dispatch({ type: SHOW_ERROR, text });
    };
    export const hideError = () => {
        getStore().dispatch({ type: HIDE_ERROR });
    };

    export const setError = (id, error, store) => {
        store.dispatch({ type: SET_ERROR, id, error });
    };
    export const clearError = (id, store) => {
        store.dispatch({ type: CLEAR_ERROR, id });
    };
