
    import React, { Component, PropTypes as pt } from 'react';
    import { connect } from 'react-redux';
    import { browserHistory } from 'react-router';
    import { addCartItem, removeCartItem, decCartItem, decRemoveCartItem, incCartItem } from 'api/cart';
    import { setHtmlTitle } from 'tools';

    const reg = new RegExp(/.*\/([a-z]+)$/, '');
    const redirect = (url, res = null) => {
        if (res) {
            res.redirect(301, url);
        } else {
            browserHistory.push({
                pathname: url,
                state: { noScroll: true }
            });
        }
    };

    class API extends Component {
        static contextTypes = {
            store: pt.object,
            res: pt.object,
            client: pt.object
        }
        static propTypes = {
            url: pt.string,
            modelId: pt.string,
            tree: pt.object,
            params: pt.object,
            location: pt.object
        }
        componentWillMount() { this.calcProps(this.props, this.context); }
        calcProps(props, context) {
            const { store, res, client } = context;
            const { location, params, tree } = props;
            const { splat } = params;
            const model = tree.models[params.modelId];
            const modelId = model !== undefined ? params.modelId : tree.products[params.modelId].models[0];
            const method = reg.exec(location.pathname)[1];
            let url = `/${splat}/${modelId}`;

            if (splat === 'order') { url = '/order'; }

            switch (method) {
                case 'add': {
                    setHtmlTitle('');
                    addCartItem(modelId, store, client);
                    break;
                }
                case 'rm': {
                    setHtmlTitle('');
                    removeCartItem(modelId, store, client);
                    break;
                }
                case 'inc': {
                    setHtmlTitle('');
                    incCartItem(modelId, store, client);
                    break;
                }
                case 'dec': {
                    setHtmlTitle('');
                    decCartItem(modelId, store, client);
                    break;
                }
                case 'decrm': {
                    setHtmlTitle('');
                    decRemoveCartItem(modelId, store, client);
                    break;
                }
                default:
                    break;
            }
            redirect(`${url}`, res);
        }
        render() { return <div />; }
    }

    const mapStateToProps = state => ({ tree: state.tree });

    export default connect(mapStateToProps)(API);
