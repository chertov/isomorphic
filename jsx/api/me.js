
    import { LOGIN_ME, LOGOUT_ME, GET_ME, NEWPASSWORD_ME } from 'reducers/me';

    export const getMe = (store, client) => {
        store.dispatch({ type: GET_ME, store, client });
    };

    export const LOGIN_ERROR = 'LOGIN_ERROR';
    export const loginMe = (phone, password, store, client, callback = () => {}, errorId = LOGIN_ERROR) => {
        store.dispatch({ type: LOGIN_ME, store, client, phone, password, errorId, callback });
    };
    export const logoutMe = (store, client, callback = () => {}, errorId = LOGIN_ERROR) => {
        store.dispatch({ type: LOGOUT_ME, store, client, callback, errorId });
    };
    export const newPasswordMe = (phone, store, client, callback = () => {}, errorId = LOGIN_ERROR) => {
        store.dispatch({ type: NEWPASSWORD_ME, phone, store, client, callback, errorId });
    };
