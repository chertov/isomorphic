
    import { GET_PRODUCTS } from 'reducers/products';
    import getStore from './../store';

    export const getProducts = () => {
        getStore().dispatch({ type: GET_PRODUCTS });
    };
