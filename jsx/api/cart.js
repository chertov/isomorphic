
    import { changeOrder } from 'api/order';
    import getStore from './../store';

    export const addCartItem = (modelId, store = getStore(), client) => {
        const order = Object.assign({}, store.getState().order);
        let find = false;

        order.items = order.items.map(cartitem => {
            const item = Object.assign({}, cartitem);

            if (item.modelId === modelId) {
                find = true;
                if (item.count < 1) { item.count = 1; }
            }
            return item;
        });
        if (!find) { order.items.push({ modelId, count: 1 }); }
        changeOrder(order, store, client);
    };
    export const upsertCartItem = (modelId, count, store = getStore(), client) => {
        let order = Object.assign({}, store.getState().order);
        let find = false;

        order.items = order.items.map(cartitem => {
            const item = Object.assign({}, cartitem);

            if (item.modelId === modelId) {
                find = true;
                item.count = count;
                if (item.count < 0) { item.count = 0; }
            }
            return item;
        });
        if (!find) { order.items.push({ modelId, count }); }
        changeOrder(order, store, client);
    };
    export const removeCartItem = (modelId, store = getStore(), client) => {
        const order = Object.assign({}, store.getState().order);

        order.items = order.items.filter(item => item.modelId !== modelId);
        changeOrder(order, store, client);
    };
    export const incCartItem = (modelId, store = getStore(), client) => {
        const order = Object.assign({}, store.getState().order);

        order.items = order.items.map(cartitem => {
            const item = Object.assign({}, cartitem);

            if (item.modelId === modelId) {
                item.count++;
                if (item.count < 0) { item.count = 0; }
            }
            return item;
        });
        changeOrder(order, store, client);
    };
    export const decCartItem = (modelId, store = getStore(), client) => {
        const order = Object.assign({}, store.getState().order);

        order.items = order.items.map(cartitem => {
            const item = Object.assign({}, cartitem);

            if (item.modelId === modelId) {
                item.count--;
                if (item.count < 0) { item.count = 0; }
            }
            return item;
        });
        changeOrder(order, store, client);
    };
    export const decRemoveCartItem = (modelId, store = getStore(), client) => {
        const order = Object.assign({}, store.getState().order);

        order.items = order.items.map(cartitem => {
            const item = Object.assign({}, cartitem);

            if (item.modelId === modelId) {
                item.count--;
                if (item.count < 0) { item.count = 0; }
            }
            return item;
        }).filter(item => item.count > 0);
        changeOrder(order, store, client);
    };
