
    import { SHOW_LOADER, HIDE_LOADER } from 'reducers/activeLoaders';

    export const showLoader = (id, store) => {
        store.dispatch({ type: SHOW_LOADER, id });
    };
    export const hideLoader = (id, store) => {
        store.dispatch({ type: HIDE_LOADER, id });
    };
