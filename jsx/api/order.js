
    import { GET_ORDER, CHANGE_ORDER, VALIDATE_ORDER, UPDATE_ORDER, MAKE_ORDER } from 'reducers/order';

    export const ERROR_ORDER = 'ERROR_ORDER';
    export const getOrder = (store, client, errorId = ERROR_ORDER) => {
        store.dispatch({ type: GET_ORDER, client, errorId });
    };
    export const changeOrder = (order, store, client, errorId = ERROR_ORDER) => {
        store.dispatch({ type: CHANGE_ORDER, store, order, client, errorId });
    };
    export const updateOrder = (order, store, client, errorId = ERROR_ORDER) => {
        store.dispatch({ type: UPDATE_ORDER, store, order, client, errorId });
    };
    export const makeOrder = (store, client, callback = () => {}, errorId = ERROR_ORDER) => {
        const { order } = store.getState();

        store.dispatch({ type: MAKE_ORDER, order, store, client, callback, errorId });
    };

    export const validateOrder = (store, callback = () => {}, errorId = ERROR_ORDER) => {
        store.dispatch({ type: VALIDATE_ORDER, store, callback, errorId });
    };
