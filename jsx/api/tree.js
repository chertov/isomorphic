
    import { GET_TREE } from 'reducers/tree';

    export const getTree = (store, client) => {
        store.dispatch({ type: GET_TREE, client });
    };
