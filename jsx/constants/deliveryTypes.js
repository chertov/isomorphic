const DELIVERY_TYPES = {
    pochtaRossii: {
        value: 'pochta-rossii',
        title: 'Почта России',
        description: 'Оплата обычно только наличными. Почта берёт комиссию за наложенный платёж.',
        image: '/images/delivery-pr.png'
    }
    /* pickPoint: {
        value: 'pick-point',
        title: 'Постаматы и пункты выдачи',
        description: 'В постаматах оплата наличными или картой. В пункте выдачи — обычно только наличными.',
        image: '/images/delivery-pp.png'
    },
    samovyvoz: {
        value: 'samovyvoz',
        title: 'Самовывоз',
        description: 'Оплата наличными'
    }*/
};

export default DELIVERY_TYPES;
