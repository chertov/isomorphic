
    const errorMessages = {
        PHONE_EXISTS_USER_ISNOT_AUTH: 'Данный телефон уже сохранён в базе данных. Пожалуйста, авторизируйтесь.',
        PHONE_BELONGS_TO_ANOTHER_USER: 'Данный телефон принадлежит другому пользователю. Пожалуйста, перелогинтесь.',
        PHONE_NOT_FOUND: 'Телефон не найден',
        PHONE_ISNT_VALID: 'Номер мобильного телефона должен начинаться с +7 и иметь 10 цифр',
        USER_PASSWORD_INCORRECT: 'Пароль некорректен',
        INN_ISNT_VALID: 'Инн содержит некорректное количество цифр. Допустимо 10 или 12 для ИП',
        KPP_ISNT_VALID: 'КПП должен содержать 9 цифр.',
        OKPO_ISNT_VALID: 'ОКПО должен содержать 8 или 10 цифр.',
        BIK_ISNT_VALID: 'БИК должен содержать 9 цифр и начинаться с 04ххххххх.',
        KORR_ISNT_VALID: 'Корреспондентский счет должен содержать 20 цифр и начинаться с 301ххххххх.',
        RASCH_ISNT_VALID: 'Расчетный счет должен содержать 20 цифр',
        DEFAULT: 'Ошибка'
    };

    export const getErrorMsg = (errors, errorsIds) => {
        if (errors === undefined) { return null; }
        if (errors === undefined) { return null; }
        const ids = Array.isArray(errorsIds) ? errorsIds : [errorsIds];
        const activeErrors = Array.isArray(errors) ? errors : [errors];
        const error = activeErrors.find(err => ids.find(errId => errId === err.name) !== undefined);

        return error !== undefined ? errorMessages[error.name] : null;
    };

    export default errorMessages;
