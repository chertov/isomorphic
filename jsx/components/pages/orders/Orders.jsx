import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { getPlural } from 'tools';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { OrdersList } from '../../ui/OrdersList';

import './page-orders.less';

class Orders extends Component {
    static propTypes = {
        me: pt.object
    };

    render() {
        const { me } = this.props;
        const orders = me.ordershistory;

        return (
            <Container>
                <Title tag='h1'>
                    История заказов
                    {orders.length > 0
                        && `. У\u00a0вас ${orders.length}\u00a0${getPlural(orders.length, ['заказ', 'заказа', 'заказов'])}`
                    }
                </Title>
                <OrdersList orders={orders} />
            </Container>
        );
    }
}

// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ me: state.me });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Orders);
