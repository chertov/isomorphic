import React, { Component } from 'react';
import { Container } from '../../ui/Container';
// import { Carousel } from '../../ui/Carousel';
import { Title } from '../../ui/Title';
import { Paragraph } from '../../ui/Paragraph';
import { OnlaysShowcase } from '../../ui/OnlaysShowcase';
// import { Link as ReactRouterLink } from 'react-router';

import './page-main.less';

class Main extends Component {

    render() {
        return (
            <Container>

                {/* <Carousel
                    className='page-main__carousel'
                    items={[
                        <ReactRouterLink
                            key='page-main__banner_1'
                            className='page-main__banner_1'
                            to='/armchair'
                        >
                            <div className='page-main__banner_1-title'>
                                Серьёзной&nbsp;проблеме&nbsp;— серьёзное&nbsp;решение.
                            </div>
                            <div className='page-main__banner_1-text'>
                                <span className='ui-link'>Ортопедическое кресло Селиванова.</span>
                                <ul className='page-main__banner_1-list'>
                                    <li>Медицински обоснованная конструкция</li>
                                    <li>Против заболеваний позвоночника,
                                        простатита, геморроя,
                                        хронической дыхательной недостаточности</li>
                                    <li>Регулировка элементов</li>
                                </ul>
                            </div>
                        </ReactRouterLink>,
                        <ReactRouterLink
                            key='page-main__banner_2'
                            className='page-main__banner_2'
                            to='/armchair'
                        >
                            <div className='page-main__banner_2-title'>
                                Всем офисом — дешевле.
                            </div>
                            <div className='page-main__banner_2-text'>
                                Скидка при заказе от 4 офисных накладок
                            </div>
                            <div className='page-main__banner_2-sale'>
                                −20%
                            </div>
                        </ReactRouterLink>
                    ]}
                />*/}

                <Title tag='h1'>
                    Популярные накладки
                </Title>

                <OnlaysShowcase />

                <Title tag='h3'>
                    Добро пожаловать в интернет-магазин ортопедических изделий!
                </Title>

                <Paragraph>
                    Интернет-магазин ортопедических изделий «Никамиста»
                    рад познакомить вас с различными товарами для здоровья позвоночника.
                    Все предлагаемая продукция полностью отечественного производства и, что ещё важнее, отечественной же разработки.
                    Подробнее о предлагаемой продукции вы можете прочесть в соответствующем разделе.
                </Paragraph>

                <Paragraph>
                    На нашем сайте представлен широкий ассортимент ортопедических товаров.
                    Для некоторых изделий существует множество различных названий,
                    но по сути это одно и то же ортопедическое приспособление:
                    например, ортопедическая накладка Селиванова,
                    она же ортопедическая накладка на стул (ортопедическая накладка на кресло),
                    она же подушка от геморроя (противогеморройная подушка),
                    она же подушка для водителя (подушка на сиденье автомобиля)
                </Paragraph>

                <Paragraph>
                    В качестве более более комплексного, эффективного и серьёзного решения
                    мы предлагаем рассмотреть специальное ортопедическое кресло Селиванова.
                </Paragraph>

                <Paragraph>
                    Для автомобилистов, проводящих много времени за рулём, особенно для дальнобойщиков и таксистов, рекомендуются
                    к использованию специальные автомобильные накладки на сиденье (автомобильные подушки)
                    и так называемые поддерживающие накладки для поясницы на спинку сиденья.
                </Paragraph>

                <Paragraph>
                    Для массажных салонов организовано производство
                    подушек, накладок и валиков для массажа различных форм и размеров.
                </Paragraph>
            </Container>
        );
    }
}

export default Main;
