import React, { Component } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { Container } from '../../ui/Container';
import { Tile } from '../../ui/Tile';
import { Title } from '../../ui/Title';
import { Price } from '../../ui/Price';
import { Paragraph } from '../../ui/Paragraph';
import { Button } from '../../ui/Button';
import { IconCart } from '../../ui/Icon/icons';

import './page-product.less';

class Product extends Component {

    render() {
        const product = {
            title: 'Офисная',
            href: '/onlays/basic',
            image: '/images/onlay_1.jpg',
            summary: 'Самая популярная модель',
            description: 'Используется ежедневно во время работы за компьютером, осуществления любой деятельности в сидячем положении.'
                + ' Также подходит для автомобильных сидений с ортопедическим рельефом.'
                + ' В группе офисных накладок возможен выбор по цене, цвету, жесткости, толщине.',
            price: 1480
        };

        return (
            <Container>

                <Tile className='page-product__content'>
                    <div className='page-product__flex'>
                        <div className='page-product__image-wrapper'>
                            <img
                                alt={product.title}
                                src={product.image}
                                className='page-product__image'
                            />
                        </div>

                        <div className='page-product__info-wrapper' >
                            <div className='page-product__breadcrumbs' >
                                <ReactRouterLink
                                    className='ui-link'
                                    to='/onlays'
                                >
                                    Накладки
                                </ReactRouterLink>
                            </div>

                            <Title tag='h1' className='page-product__title'>
                                {product.title}
                            </Title>

                            <Price value={product.price} className='page-product__price' />

                            <Button className='page-product__button'>
                                <IconCart
                                    className='page-product__button-icon'
                                    fill='fountain'
                                />
                                Добавить в&nbsp;корзину
                            </Button>

                            <div className='page-product__description page-product__description_desktop' >
                                <Paragraph hyphenated>
                                    {product.description}
                                </Paragraph>
                                <Paragraph hyphenated>
                                    {product.description}
                                </Paragraph>
                            </div>
                        </div>
                    </div>

                    <div className='page-product__description page-product__description_mobile' >
                        <Paragraph hyphenated>
                            {product.description}
                        </Paragraph>
                        <Paragraph hyphenated>
                            {product.description}
                        </Paragraph>
                    </div>
                </Tile>

            </Container>
        );
    }
}

export default Product;
