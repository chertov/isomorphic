import React, { Component } from 'react';
import { setHtmlTitle } from 'tools';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { Link } from '../../ui/Link';
import { Gap } from '../../ui/Gap';
import { IconClock, IconComission, IconPackage, IconSMS, IconWallet } from '../../ui/Icon/icons';

import './page-delivery.less';

import DELIVERY_TYPES from '../../../constants/deliveryTypes';

class Delivery extends Component {

    render() {
        setHtmlTitle('Доставка и оплата');

        return (
            <Container>

                <Title tag='h1'>
                    Доставка и&nbsp;оплата
                </Title>

                <div className='page-delivery__options'>

                    {DELIVERY_TYPES.pochtaRossii
                        && <div className='page-delivery__option'>
                            <div className='page-delivery__option-content'>
                                <img
                                    alt='Почта России'
                                    src='/images/delivery-pr.png'
                                    className='page-delivery__option-image'
                                />
                                <div className='page-delivery__option-title'>
                                    Доставка Почтой&nbsp;России
                                </div>

                                <div className='page-delivery__option-description'>
                                    Отправляем посылку с&nbsp;<Link
                                        external
                                        addExternalIcon
                                        href='https://www.pochta.ru/support/post-rules/cash-on-delivery'
                                    >
                                    наложенным платежом
                                </Link>.
                                    Вы&nbsp;получаете посылку в&nbsp;своём отделении,
                                    оплачивая стоимость заказа и&nbsp;комиссию Почты России.
                                </div>

                                <ul className='page-delivery__option-list'>
                                    <li className='page-delivery__option-list-item'>
                                        <IconWallet className='page-delivery__option-list-item-image' />
                                        Без предоплаты
                                    </li>
                                    <li className='page-delivery__option-list-item'>
                                        <IconPackage className='page-delivery__option-list-item-image' />
                                        Оплата при&nbsp;получении
                                    </li>
                                    <li className='page-delivery__option-list-item'>
                                        <IconComission /* fill='chestnut' */ className='page-delivery__option-list-item-image' />
                                        Комиссия Почты за&nbsp;наложенный платёж
                                    </li>
                                </ul>

                                <div className='page-delivery__option-payment'>
                                    Оплата производится обычно только наличными.
                                </div>
                            </div>
                        </div>
                    }

                    {DELIVERY_TYPES.pickPoint
                        && <div className='page-delivery__option'>
                            <div className='page-delivery__option-content'>
                                <img
                                    alt='PickPoint'
                                    src='/images/delivery-pp.png'
                                    className='page-delivery__option-image page-delivery__option-image_pickpoint'
                                />
                                <div className='page-delivery__option-title'>
                                    Постаматы и&nbsp;пункты выдачи PickPoint
                                </div>

                                <div className='page-delivery__option-description'>
                                    Отправляем посылку в&nbsp;<Link
                                        external
                                        addExternalIcon
                                        href='https://pickpoint.ru/postamats/'
                                    >
                                    ближайший к&nbsp;вам постамат
                                </Link> или&nbsp;пункт выдачи заказов PickPoint.
                                </div>

                                <ul className='page-delivery__option-list'>
                                    <li className='page-delivery__option-list-item'>
                                        <IconWallet className='page-delivery__option-list-item-image' />
                                        Без предоплаты
                                    </li>
                                    <li className='page-delivery__option-list-item'>
                                        <IconPackage className='page-delivery__option-list-item-image' />
                                        Оплата при&nbsp;получении
                                    </li>
                                    <li className='page-delivery__option-list-item'>
                                        <IconClock className='page-delivery__option-list-item-image' />
                                        Быстро и&nbsp;в&nbsp;удобное время
                                    </li>
                                    <li className='page-delivery__option-list-item'>
                                        <IconSMS className='page-delivery__option-list-item-image' />
                                        Оповещение по&nbsp;SMS и&nbsp;звонком
                                    </li>
                                </ul>

                                <div className='page-delivery__option-payment'>
                                    Оплата в постаматах производится наличными или&nbsp;картой.
                                    <Gap size='xs' />
                                    В&nbsp;пунктах выдачи — обычно наличными.
                                    Возможность оплаты картой, пожалуйста, уточняйте в&nbsp;вашем пункте выдачи.
                                </div>

                                { /* <div className='page-delivery__option-soon'>
                                    <div className='page-delivery__option-soon-text'>
                                        Скоро
                                    </div>
                                </div> */ }
                            </div>
                        </div>
                    }

                </div>

            </Container>
        );
    }
}

export default Delivery;
