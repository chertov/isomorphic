import React, { Component } from 'react';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { Paragraph } from '../../ui/Paragraph';
import { Columns } from '../../ui/Columns';
import { OnlaysShowcase } from '../../ui/OnlaysShowcase';
import { Gap } from '../../ui/Gap';

import './page-armchairs.less';

class Armchairs extends Component {

    render() {
        return (
            <Container>
                <Title tag='h1'>
                    Кресло Селиванова
                </Title>

                <Columns>
                    <Paragraph hyphenated>
                        Меняющийся угол наклона спинки и регулируемый по высоте брус и на спинке и на сиденьи служат для получения максимального разгрузочного эффекта в зависимости от роста человека, особенностей его телосложения и проблем в области малого таза.
                    </Paragraph>
                </Columns>

                <Gap size='m' />

                <OnlaysShowcase className='page-onlays__showcase' />

                <Title tag='h2'>
                    Скамья Селиванова
                </Title>

                <Columns>
                    <Paragraph hyphenated>
                        Специализированная профилактическая накладка Селиванова выполнена из экологически чистых материалов (пенополиуретан, пенолон).
                        Такие материалы используются при изготовлении изделий медицинского назначения.
                    </Paragraph>
                </Columns>

            </Container>
        );
    }
}

export default Armchairs;
