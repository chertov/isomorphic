import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { setHtmlTitle } from 'tools';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { Paragraph } from '../../ui/Paragraph';
import { Columns } from '../../ui/Columns';
import { ProductList } from '../../ui/ProductList';
import { Tile } from '../../ui/Tile';
import { RadioTags } from '../../ui/RadioTags';

import './page-onlays.less';

class Onlays extends Component {

    static propTypes = {
        tree: pt.object
    };

    state={ filter: 'all' };

    handleFilter = value => {
        this.setState({
            filter: value
        });
    };

    render() {
        setHtmlTitle('Накладки Селиванова');

        const { tree } = this.props;
        const { filter } = this.state;
        const productsOffice = tree.categories.common.products;
        const productsCar = tree.categories.automobile.products;

        let products = [];

        switch (filter) {
            case 'all':
                products = productsCar.concat(productsOffice);
                break;
            case 'office':
                products = productsOffice;
                break;
            case 'car':
                products = productsCar;
                break;
            default:
                products = [];
        }

        return (
            <Container>
                <Title tag='h1'>
                    Накладки Селиванова
                </Title>

                <Columns>
                    <Paragraph hyphenated>
                        Специализированная профилактическая накладка Селиванова выполнена из экологически чистых материалов (пенополиуретан, пенолон).
                        Такие материалы используются при изготовлении изделий медицинского назначения.
                    </Paragraph>
                    <Paragraph hyphenated>
                        Специализированная профилактическая накладка Селиванова на стул и водительское сиденье является запатентованным изобретением российского производства.
                        Накладка Селиванова была одобрена врачами и специалистами, получила Золотую Медаль на Форуме инноваций и достижений «Архимед» в 2008 году.
                    </Paragraph>
                    <Paragraph hyphenated>
                        Специализированная профилактическая накладка Селиванова имеет все необходимые сертификаты и документы.
                    </Paragraph>
                </Columns>

                <Title tag='h2'>
                    Каталог накладок
                </Title>

                <Tile>
                    <RadioTags
                        name='onlays-filter'
                        options={[
                            { title: 'Все', id: 'all' },
                            { title: 'Для дома и офиса', id: 'office' },
                            { title: 'Для автомобиля', id: 'car' }
                            // { title: 'Со скидкой', id: 'discount' }
                        ]}
                        value='all'
                        hint='Для больших кресел часто берут автомобильные накладки.'
                        onChange={this.handleFilter}
                    />
                </Tile>

                <ProductList
                    // urlPath={'my'}
                    products={products}
                />

            </Container>
        );
    }
}


const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(Onlays);
