import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { Container } from '../../ui/Container';
import { Tile } from '../../ui/Tile';
import { Title } from '../../ui/Title';

import './page-profile.less';

class Profile extends Component {
    static propTypes = {
        me: pt.object,
        children: pt.element
    };
    render() {
        const { me } = this.props;

        if (!me) {
            return <Container>
                <Title tag='h1'>
                    Пожалуйста, авторизуйтесь
                </Title>

                <Tile>
                    Данная страница доступна только для авторизованных пользователей.
                    <br />
                    <br />
                    Пожалуйста, войдите с паролем.
                </Tile>
            </Container>;
        }
        return this.props.children;
    }
}

export default connect(state => ({ me: state.me }))(Profile);
