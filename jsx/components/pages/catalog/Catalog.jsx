import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { setHtmlTitle } from 'tools';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { CategoryList } from '../../ui/CategoryList';
import { ProductList } from '../../ui/ProductList';
import { Product } from '../../ui/Product';
import { Breadcrumbs } from '../../ui/Breadcrumbs';
import { Gap } from '../../ui/Gap';

import './page-catalog.less';

class Catalog extends Component {
    static propTypes = {
        tree: pt.object,
        params: pt.object
    };

    static childContextTypes = {
        urlPath: pt.string
    };

    constructor(props, context) {
        super(props, context);
        this.state = this.calcProps(props, context);
    }

    getChildContext() {
        return { urlPath: this.state.urlPath };
    }

    componentWillReceiveProps(newProps) {
        this.setState(this.calcProps(newProps, this.context));
    }

    calcProps(props, context) {
        const { tree } = props;
        let pathPrefix = props.route.pathPrefix;

        if (!pathPrefix) { pathPrefix = 'catalog'; }
        pathPrefix = `/${pathPrefix}/`;

        const { c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19 } = props.params;
        const path = [c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19].filter(n => typeof (n) !== 'undefined');  // remove all undefined elements
        let urlPath = `${pathPrefix}${path.join('/')}/`.replace('///', '/').replace('//', '/');
        let node = tree.root; // for now use root of "tree", but can be any node / folder / category
        let lvl = 0;
        let isRoot = false;
        let isFolder = false;
        let isCategory = false;
        let isProduct = false;
        let modelId = '';
        const breadcrumbs = [{ title: 'Каталог', path: '' }];

        if (path.length === 0) {
            isRoot = true;
        } else {
            while (lvl < path.length) {
                const id = path[lvl];

                lvl += 1;

                if (typeof node.folders !== 'undefined') {
                    const folder = tree.folders[node.folders.find(f => f === id)];

                    if (folder) {
                        node = folder;
                        breadcrumbs.push({ title: node.title, path: `${path.slice(0, lvl).join('/')}` });
                        isFolder = true; isCategory = false; isProduct = false;
                        continue;
                    }
                }
                if (typeof node.categories !== 'undefined') {
                    const category = tree.categories[node.categories.find(c => c === id)];

                    if (category) {
                        node = category;
                        breadcrumbs.push({ title: node.title, path: `${path.slice(0, lvl).join('/')}` });
                        isFolder = false; isCategory = true; isProduct = false;
                        continue;
                    }
                }
                if (typeof node.products !== 'undefined') {
                    node = tree.products[node.products.find(product => product === id)];
                    if (node) {
                        breadcrumbs.push({ title: node.title, path: `${path.slice(0, lvl).join('/')}` });
                        isFolder = false; isCategory = false; isProduct = true;
                        continue;
                    }
                }
                if (typeof node.models !== 'undefined') {
                    const model = tree.models[node.models.find(m => m === id)];

                    if (model) {
                        urlPath = `${pathPrefix}${breadcrumbs[breadcrumbs.length - 1].path}/`;
                        isFolder = false; isCategory = false; isProduct = true;
                        modelId = id;
                        continue;
                    }
                }
                break;
            }
        }

        // breadcrumbs = breadcrumbs.slice(0, breadcrumbs.length - 1);

        return {
            node,
            isRoot,
            isFolder,
            isCategory,
            isProduct,
            modelId,
            pathPrefix,
            urlPath,
            breadcrumbs
        };
    }

    render() {
        const { node, isRoot, isFolder, isCategory, isProduct, modelId, pathPrefix, urlPath, breadcrumbs } = this.state;

        const breadcrumbsComp = <Breadcrumbs
            noTile
            items={breadcrumbs}
            pathPrefix={pathPrefix}
        />;
        const titleComp = <Title tag='h1' className='page-catalog__title'>
            {node.title}
        </Title>;
        let folders = <div />;
        let categories = <div />;

        if (isRoot || isFolder) {
            folders = <CategoryList
                urlPath={urlPath}
                folder
                items={node.folders}
            />;
            categories = <CategoryList
                urlPath={urlPath}
                items={node.categories}
            />;
        }
        setHtmlTitle('Каталог магазина');

        if (isRoot) {
            return <Container>
                {folders}
                <Gap size='m' />
                {categories}
            </Container>;
        }
        if (isFolder) {
            setHtmlTitle(`${node.title}`);
            return <Container>
                {breadcrumbsComp}
                {titleComp}
                {folders}
                <Gap size='m' />
                {categories}
            </Container>;
        }
        if (isCategory) {
            setHtmlTitle(`${node.title}`);
            return <Container>
                {breadcrumbsComp}
                {titleComp}
                <ProductList
                    urlPath={urlPath}
                    products={node.products}
                />
            </Container>;
        }
        if (isProduct) {
            setHtmlTitle(`${node.title}`);
            return <Container>
                {breadcrumbsComp}
                <Product
                    product={node}
                    modelId={modelId}
                />
            </Container>;
        }
        return <div> {'Not found'} </div>;
    }
}


const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(Catalog);
