import React, { Component, PropTypes as pt } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { connect } from 'react-redux';

import { Container } from 'components/ui/Container';
import { Title } from 'components/ui/Title';
import { Tile } from 'components/ui/Tile';
import { Gap } from 'components/ui/Gap';

import './page-ordersuccess.less';

import DELIVERY_TYPES from '../../../constants/deliveryTypes';

class OrderSuccess extends Component {
    static propTypes = {
        me: pt.object,
        params: pt.object
    };

    render() {
        const { me } = this.props;
        const { ordernumber } = this.props.params;

        if (!me || !ordernumber) { return null; }
        const orders = me.ordershistory;

        if (!orders) { return null; }
        const successOrder = orders.find(order => order.number.toString() === ordernumber);
        const deliveryType = successOrder.delivery && successOrder.delivery.type;

        let deliveryInfo = '';

        switch (deliveryType) {
            case DELIVERY_TYPES.pickPoint.value:
                deliveryInfo = <div>Служба доставки PickPoint известит вас о&nbsp;прибытии посылки в&nbsp;пункт выдачи заказов по&nbsp;СМС или&nbsp;звонком.</div>;
                break;
            /* case DELIVERY_TYPES.pochtaRossii.value:
                deliveryInfo = <div></div>;
                break; // ------------------------------------- Почта России - то же, что и default */
            default:
                deliveryInfo = <div>Когда заказ прибудет в&nbsp;место выдачи, мы&nbsp;известим вас по&nbsp;СМС.</div>;
        }

        return (
            <Container>
                <Title tag='h1'>
                    Заказ успешно оформлен!
                </Title>
                <Tile>

                    Номер вашего заказа: <span className='page-ordersuccess__number'>1345134{ordernumber}</span>

                    <Gap />

                    {orders.length
                        && <div>
                            Пароль, полученный в&nbsp;СМС, вы&nbsp;можете использовать в&nbsp;дальнейшем для&nbsp;входа в&nbsp;Личный&nbsp;кабинет.
                        </div>
                    }

                    <Gap />

                    {deliveryInfo}

                    <Gap />

                    Вы всегда сами можете проверить статус заказа на&nbsp;странице
                    {' '}
                    <ReactRouterLink className='ui-link' to='/profile/orders'>История заказов</ReactRouterLink>
                    {' '}
                    в&nbsp;Личном&nbsp;кабинете.

                </Tile>
            </Container>
        );
    }
}

const mapStateToProps = state => ({ me: state.me });

export default connect(mapStateToProps)(OrderSuccess);
