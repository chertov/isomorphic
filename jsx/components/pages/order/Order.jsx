import React, { Component } from 'react';

import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { OrderList } from '../../ui/OrderList';
import { OrderDelivery } from '../../ui/OrderDelivery';
import { OrderForm } from '../../ui/OrderForm';

import './page-order.less';

class Order extends Component {
    render() {
        return (
            <Container>
                <Title id='form' tag='h1'>
                    Оформление заказа
                </Title>

                <OrderList />

                <OrderDelivery />

                <OrderForm />
            </Container>
        );
    }
}

export default Order;
