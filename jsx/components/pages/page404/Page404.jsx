import React, { Component } from 'react';
import { browserHistory, Link as ReactRouterLink } from 'react-router';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { Tile } from '../../ui/Tile';
import { Link } from '../../ui/Link';

import './page-404.less';

class Page404 extends Component {

    handleGoBack = event => {
        event.preventDefault();
        browserHistory.goBack();
    };

    render() {
        return (
            <Container>

                <Title tag='h1'>
                    Кажется, что-то <span className='nobr'>пошло не так<sub className='page-404__sub'>404</sub></span>
                </Title>

                <Tile>
                    <Title tag='h3' className='page-404__subtitle'>
                        Мы не можем найти запрошенную страницу.
                    </Title>

                    {'Можете вернуться '}
                    <Link onClick={this.handleGoBack}>
                        на&nbsp;предыдущую страницу
                    </Link>
                    {', перейти'}&nbsp;
                    <ReactRouterLink to='/' className='ui-link'>
                        на&nbsp;главную
                    </ReactRouterLink>
                    {' или'}&nbsp;
                    <ReactRouterLink to='/catalog' className='ui-link'>
                        в&nbsp;каталог
                    </ReactRouterLink>.
                </Tile>


            </Container>
        );
    }
}

export default Page404;
