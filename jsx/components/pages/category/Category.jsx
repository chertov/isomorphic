import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { Link as ReactRouterLink } from 'react-router';
import { Container } from '../../ui/Container';
import { Tile } from '../../ui/Tile';
import { Title } from '../../ui/Title';
import { CategoryList } from '../../ui/CategoryList';

import './page-category.less';

class Category extends Component {

    static propTypes = {
        tree: pt.object
    };

    render() {
        const { tree } = this.props;
        const currentNode = tree; // for now use root of "tree", but can be any node / folder / category

        console.log('----------- tree');
        console.log(tree);
        console.log('----------- currentNode');
        console.log(currentNode);

        return (
            <Container>
                <Title tag='h1'>
                    Ноутбуки
                </Title>


                <Tile className='page-category__breadcrumbs'>
                    <ReactRouterLink
                        className='ui-link'
                        to='/onlays'
                    >
                        Электроника
                    </ReactRouterLink>
                    {' / '}
                    <ReactRouterLink
                        className='ui-link'
                        to='/onlays'
                    >
                        Ноутбуки
                    </ReactRouterLink>
                </Tile>

                <CategoryList
                    categories={currentNode.categories}
                />

            </Container>
        );
    }
}


const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(Category);
