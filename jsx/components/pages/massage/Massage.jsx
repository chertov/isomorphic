import React, { Component } from 'react';
import { Container } from '../../ui/Container';
import { Title } from '../../ui/Title';
import { Paragraph } from '../../ui/Paragraph';
import { Columns } from '../../ui/Columns';
import { ProductList } from '../../ui/ProductList';
import { Gap } from '../../ui/Gap';

import './page-massage.less';

class Massage extends Component {

    render() {
        return (
            <Container>
                <Title tag='h1'>
                    Валики и подушки для массажа
                </Title>

                <Columns>
                    <Paragraph hyphenated>
                        Специализированная профилактическая накладка Селиванова выполнена из экологически чистых материалов (пенополиуретан, пенолон).
                        Такие материалы используются при изготовлении изделий медицинского назначения.
                    </Paragraph>
                </Columns>

                <Gap size='m' />

                <ProductList
                    products={[
                        {
                            title: 'Офисная',
                            href: '/onlays/basic',
                            image: '/images/onlay_1.jpg',
                            summary: 'Самая популярная модель',
                            description: 'Самая популярная модель',
                            price: 1480
                        },
                        {
                            title: 'Переносная',
                            href: '/onlays/basic',
                            image: '/images/onlay_1.jpg',
                            summary: 'Складывается пополам',
                            description: 'Самая популярная модель',
                            price: 1630
                        },
                        {
                            title: 'Детская',
                            href: '/onlays/basic',
                            image: '/images/onlay_1.jpg',
                            summary: 'Заботимся о позвоночнике с детства',
                            description: 'Самая популярная модель',
                            price: 1210
                        },
                        {
                            title: 'Автомобильная',
                            href: '/onlays/basic',
                            image: '/images/onlay_1.jpg',
                            summary: 'Для водителей',
                            description: 'Длинное описание',
                            price: 1510
                        }
                    ]}
                />

            </Container>
        );
    }
}

export default Massage;
