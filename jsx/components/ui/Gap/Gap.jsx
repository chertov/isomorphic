import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-gap.less';

class Gap extends Component {
    static propTypes = {
        size: pt.string,
        className: pt.string
    };

    static defaultProps = {
        size: 's'
    };

    render() {
        const { size, className } = this.props;

        return (
            <div
                className={classNames({
                    'ui-gap': true,
                    [`ui-gap_size_${size}`]: size,
                    [className]: className
                })}
            />
        );
    }
}

export default Gap;
