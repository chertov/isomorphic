import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';
import { Link as ReactRouterLink } from 'react-router';
import { Tile } from '../Tile';

import './ui-breadcrumbs.less';

class Breadcrumbs extends Component {
    static propTypes = {
        items: pt.array,
        noTile: pt.bool,
        pathPrefix: pt.string,
        onClick: pt.func,
        className: pt.string
    };

    static defaultProps = {
        items: [],
        onClick: () => {}
    };

    handleClick = event => {
        this.props.onClick(event);
    };

    render() {
        const { items, noTile, pathPrefix, className } = this.props;

        const breadcrumbs = items.map((item, i) => {
            if (i !== items.length - 1) {
                return <span
                    key={item.path || i}
                    className='ui-breadcrumbs__item'
                >
                    <ReactRouterLink
                        className='ui-link'
                        to={`${pathPrefix}${item.path}`}
                    >
                        {item.title}
                    </ReactRouterLink>
                </span>;
            }
            return <span
                key={item.path || i}
                className='ui-breadcrumbs__item'
            >
                {item.title}
            </span>;
        });

        return (
            <div
                className={classNames({
                    'ui-breadcrumbs': true,
                    [className]: className
                })}
            >
                {noTile ?
                    breadcrumbs :
                    <Tile>{breadcrumbs}</Tile>
                }
            </div>

        );
    }
}

export default Breadcrumbs;
