import React, { Component, PropTypes as pt } from 'react';
import { browserHistory } from 'react-router';
import classNames from 'classnames';
import { addCartItem, upsertCartItem, incCartItem, decCartItem, decRemoveCartItem } from 'api/cart';
import { Button } from '../Button';
import { Input } from '../Input';
import { IconCart } from '../Icon/icons';

import './ui-button-buy.less';

class ButtonBuy extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        count: pt.number,
        allowZero: pt.bool,
        disabled: pt.bool,
        small: pt.bool,
        className: pt.string,
        modelUrl: pt.string,
        modelId: pt.string
    };

    static defaultProps = {
        count: 0
    };

    handleBuy = () => {
        const { modelId } = this.props;

        if (modelId) {
            const { store, client } = this.context;

            addCartItem(modelId, store, client);
        } else {
            browserHistory.push({ pathname: `${this.props.modelUrl}/add`, state: { noScroll: true } });
        }
    };

    handleInc = () => {
        const { modelId } = this.props;

        if (modelId) {
            const { store, client } = this.context;

            incCartItem(modelId, store, client);
        } else {
            browserHistory.push({ pathname: `${this.props.modelUrl}/inc`, state: { noScroll: true } });
        }
    };

    handleDec = () => {
        const { allowZero, modelUrl, modelId } = this.props;

        if (modelId) {
            const { store, client } = this.context;

            allowZero ? decCartItem(modelId, store, client) : decRemoveCartItem(modelId, store, client);
        } else {
            browserHistory.push({
                pathname: allowZero ? `${modelUrl}/dec` : `${modelUrl}/decrm`,
                state: { noScroll: true }
            });
        }
    };

    handleChange = event => {
        const count = event.target.value === '' ? 0 : parseInt(event.target.value, 10);
        const { allowZero, modelUrl, modelId } = this.props;
        const { store, client } = this.context;

        if (modelId) { upsertCartItem(modelId, count, store, client); }
        if (count === 0 && !allowZero) {
            modelId ? decRemoveCartItem(modelId, store, client) :
            browserHistory.push({
                pathname: `${modelUrl}/rm`,
                state: { noScroll: true }
            });
        }
    };

    render() {
        const { count, allowZero, disabled, small, className, modelUrl } = this.props;

        return <span
            className={classNames({
                'ui-button-buy__wrapper': true,
                [className]: className
            })}
        >
            {(count > 0 || allowZero) ?
                <span>
                    <Button
                        title='Убрать'
                        size={small && 's'}
                        className='ui-button-buy__minus'
                        onClick={this.handleDec}
                        href={allowZero ? `${modelUrl}/dec` : `${modelUrl}/decrm`}
                    >
                        −
                    </Button>
                    <Input
                        value={count}
                        type='tel'
                        pattern='[0-9]*'
                        inputmode='numeric'
                        min='0'
                        max='99'
                        onChange={this.handleChange}
                        className={classNames({
                            'ui-button-buy__input': true,
                            'ui-button-buy__input_small': small
                        })}
                    />
                    <Button
                        title='Добавить'
                        size={small && 's'}
                        className='ui-button-buy__plus'
                        onClick={this.handleInc}
                        href={`${modelUrl}/inc`}
                    >
                        +
                    </Button>

                </span> :
                <Button
                    disabled={disabled}
                    size={small && 's'}
                    className='ui-button-buy__button'
                    onClick={this.handleBuy}
                    href={`${modelUrl}/add`}
                >
                    <IconCart
                        className='ui-button-buy__button-icon'
                        fill='fountain'
                    />
                    Добавить в&nbsp;корзину
                </Button>
            }
        </span>;
    }
}

export default ButtonBuy;
