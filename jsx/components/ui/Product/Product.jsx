import React, { Component, PropTypes as pt } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { connect } from 'react-redux';
import { map } from 'ramda';
import { setHtmlTitle } from 'tools';
import { Tile } from '../Tile';
import { Title } from '../Title';
import { Price } from '../Price';
import { RadioTags } from '../RadioTags';
import { Paragraph } from '../Paragraph';
import { ButtonBuy } from '../ButtonBuy';

import './ui-product.less';


const inOrder = (order, modelId) => {
    const arr = order.items.filter(item => item.modelId === modelId);

    if (arr.length > 0) { return arr[0].count; }
    return 0;
};

class Product extends Component {
    static contextTypes = {
        urlPath: pt.string
    };

    static propTypes = {
        tree: pt.object,
        order: pt.object,
        product: pt.object,
        modelId: pt.string
    };

    getSelectedModel = () => {
        const { product, modelId } = this.props;
        const { models } = product;

        return modelId === '' ? models[0] : models.find(model => model === modelId);
    };

    render() {
        const { product, modelId, order, tree } = this.props;
        const { urlPath } = this.context;
        const { models } = product;

        // выбираем модель из ссылки или берем первую
        const selectedModel = tree.models[this.getSelectedModel()];

        // значения для общей страницы продукта
        const title = product.title;
        let description = product.description;
        let images = product.images;

        // если модель из ссылки явно указана,то заменяем параметры продукта на параметры из модели
        if (modelId !== '') {
            // if (selectedModel.title !== '') { title = selectedModel.title; }
            if (selectedModel.description !== '') { description = selectedModel.description; }
            if (!!selectedModel.images && selectedModel.images.length > 0) { images = selectedModel.images; }
        }

        const modelOptions = map(id => {
            const model = tree.models[id];

            return {
                id: `${model.id}`,
                title: model.shortTitle || model.title,
                href: `${model.id}`
            };
        }, models);

        if (!images || images.length === 0) { images = ['noimage']; }

        setHtmlTitle(`${title}`);

        const count = inOrder(order, selectedModel.id);

        return (
            <Tile
                className='ui-product__tile'
                itemScope itemType='http://schema.org/Product'
            >
                <div className='ui-product__flex'>
                    <div className='ui-product__image-wrapper'>
                        <img
                            alt={title}
                            src={`/images/items/${images[0]}.png`}
                            className='ui-product__image'
                            itemProp='image'
                        />
                    </div>

                    <div className='ui-product__info-wrapper' >
                        <Title
                            tag='h1'
                            className='ui-product__title'
                        >
                            {title}
                        </Title>

                        <meta itemProp='name' content={selectedModel.title} />

                        {models.length > 1
                            && <div>
                                <RadioTags
                                    className='ui-product__models'
                                    name='onlays-filter'
                                    title='Выберите модель:'
                                    options={modelOptions}
                                    value={`${selectedModel.id}`}
                                    hint=''
                                    urlPath={urlPath}
                                />

                                <Paragraph hyphenated>
                                    {selectedModel.summary}
                                </Paragraph>
                            </div>
                        }

                        <meta itemProp='disambiguatingDescription' content={selectedModel.summary} />

                        <Price
                            value={selectedModel.price.value}
                            className='ui-product__price'
                        />

                        <ButtonBuy
                            disabled={!selectedModel.price.value}
                            count={count}
                            modelId={selectedModel.id}
                            modelUrl={`${urlPath}${selectedModel.id}`}
                            className='ui-product__buy-button'
                        />

                        {count > 0
                            && <div className='ui-product__order-link'>
                                <div>Завершить покупки</div>
                                и <ReactRouterLink to='/order/#form' className='ui-link'>
                                    оформить заказ
                                </ReactRouterLink>
                            </div>
                        }

                        <div className='ui-product__description ui-product__description_desktop' >
                            <Paragraph hyphenated itemProp='description'>
                                {description}
                            </Paragraph>
                        </div>

                    </div>
                </div>

                <div className='ui-product__description ui-product__description_mobile' >
                    <Paragraph hyphenated>
                        {description}
                    </Paragraph>
                </div>
            </Tile>
        );
    }
}

const mapStateToProps = state => ({ order: state.order, tree: state.tree });

export default connect(mapStateToProps)(Product);
