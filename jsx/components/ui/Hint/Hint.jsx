import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';
import { IconQuestion } from '../Icon/icons';

import './ui-hint.less';

class Hint extends Component {

    static propTypes = {
        className: pt.string,
        text: pt.string,
        children: pt.node
    };

    render() {
        const { className, text, children } = this.props;

        return (
            <div
                data-tip={text}
                className={classNames({
                    'ui-hint': true,
                    [className]: className
                })}
            >
                {
                    children || <IconQuestion fill='geyser' className='ui-hint__icon' />
                }

                <ReactTooltip className='ui-hint__tooltip' />
            </div>
        );
    }
}

export default Hint;
