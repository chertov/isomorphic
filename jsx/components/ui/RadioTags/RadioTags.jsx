import React, { Component, PropTypes as pt } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import classNames from 'classnames';
import { Hint } from '../Hint';

import './ui-radio-tags.less';

class RadioTags extends Component {
    static contextTypes = {
        urlPath: pt.string
    };

    static propTypes = {
        name: pt.string,
        title: pt.string,
        inlineTitle: pt.bool,
        options: pt.array,
        value: pt.string,
        hint: pt.string,
        className: pt.string,
        onChange: pt.func
    };

    static defaultProps = {
        options: [],
        onChange: () => {}
    };

    handleChange = event => {
        this.props.onChange(event.target.value);
    };

    render() {
        const { name, title, inlineTitle, options, hint, className, value } = this.props;
        const { urlPath } = this.context;

        return (
            <div
                className={classNames({
                    'ui-radio-tags': true,
                    [className]: className
                })}
            >
                {title
                    && <div
                        className={classNames({
                            'ui-radio-tags__title': true,
                            'ui-radio-tags__title_inline': inlineTitle
                        })}
                    >
                        {title}
                    </div>
                }
                <div className='ui-radio-tags__options'>
                    {
                        options.map((option, i) => {
                            const inputid = `${name}_${option.id}`;
                            const isChecked = option.id === value;
                            const isLink = !!option.href;
                            const Tag = isLink ? ReactRouterLink : 'div';
                            const linkProps = isLink && {
                                to: `${urlPath}${option.href}`,
                                state: { noScroll: true }
                            };
                            const inputProps = isLink ?
                            {
                                checked: isChecked,
                                readOnly: true
                            } :
                            {
                                defaultChecked: isChecked,
                                onChange: this.handleChange
                            };

                            return <Tag
                                key={option.id || i}
                                className='ui-radio-tags__option'
                                {...linkProps}
                            >
                                <input
                                    id={inputid}
                                    type='radio'
                                    name={name}
                                    value={option.id}
                                    className='ui-radio-tags__input'
                                    {...inputProps}
                                />

                                <label
                                    htmlFor={inputid}
                                    className='ui-radio-tags__label'
                                >
                                    <div className='ui-radio-tags__label-content'>
                                        {option.title}
                                    </div>
                                </label>
                            </Tag>;
                        })
                    }
                </div>

                {hint
                    && <Hint
                        text={hint}
                        className='ui-radio-tags__hint'
                    />
                }
            </div>
        );
    }
}

export default RadioTags;
