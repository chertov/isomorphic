import React, { Component, PropTypes } from 'react';

import './ui-container.less';

class Container extends Component {

    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className='ui-container'>
                {this.props.children}
            </div>
        );
    }
}

export default Container;
