import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-remove-button.less';

class RemoveButton extends Component {

    static propTypes = {
        size: pt.oneOfType([pt.number, pt.string]),
        title: pt.string,
        className: pt.string,
        onClick: pt.func,
        href: pt.string
    };

    static defaultProps = {
        size: 25,
        title: 'Удалить',
        onClick: () => {}
    };

    handleClick = event => {
        this.buttonRef.blur();

        this.props.onClick(event);
    };

    render() {
        const { size, title, className, href } = this.props;
        const button = <button
            ref={ref => { this.buttonRef = ref; }}
            href={href}
            onClick={this.handleClick}
            type='button'
            title={title}
            className={classNames({
                'ui-remove-button': true,
                [className]: className
            })}
            style={{
                height: size,
                width: size
            }}
        >
            ×
        </button>;

        return href ? <a href={href} onClick={e => e.preventDefault()}>{button}</a> : button;
    }
}

export default RemoveButton;
