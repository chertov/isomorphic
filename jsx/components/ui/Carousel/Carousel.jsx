import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-carousel.less';

class Carousel extends Component {

    static propTypes = {
        className: pt.string,
        items: pt.array
    };

    render() {
        const { className, items } = this.props;

        return (
            <div
                className={classNames({
                    'ui-carousel': true,
                    [className]: className
                })}
            >
                {
                    items.map((item, i) =>
                        <div className='ui-carousel__item' key={i}>
                            {item}
                        </div>
                    )
                }
            </div>
        );
    }
}

export default Carousel;
