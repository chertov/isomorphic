import React, { Component, PropTypes as pt } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link as ReactRouterLink } from 'react-router';
import { getPlural, getUrlPath } from 'tools';
import { removeCartItem } from 'api/cart';
import { TooltipPanel } from '../TooltipPanel';
import CartRow from './CartRow.jsx';
import { Link } from '../Link';
import { IconCart } from '../Icon/icons';

import './ui-cart.less';

class Cart extends Component {
    static contextTypes = {
        store: pt.object,
        res: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object,
        tree: pt.object
    };

    static defaultProps = {
        cart: []
    };
    state = { cartCollapsed: true };

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside.bind(this), true);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside.bind(this), true);
    }

    handleClickOutside(event) {
        if (this.state.cartCollapsed) { return; }
        const domNode = ReactDOM.findDOMNode(this);

        if ((!domNode || !domNode.contains(event.target))) {
            this.setState({
                cartCollapsed: true
            });
        }
    }

    handleToggleCartBody = event => {
        event.preventDefault();
        this.setState({
            cartCollapsed: !this.state.cartCollapsed
        });
    };

    handleCloseCartBody = event => {
        this.setState({
            cartCollapsed: true
        });
    };

    render() {
        const { store, client } = this.context;
        const { order, tree } = this.props;
        const { cartCollapsed } = this.state;
        let allPrice = 0;
        let allItems = 0;
        const models = [];

        order.items.forEach(item => {
            const model = tree.models[item.modelId];

            models.push({ model, count: item.count });
            allPrice += model.price.value * item.count;
            allItems += item.count;
        });

        const isEmpty = !models.length;
        const productsStr = getPlural(allItems, ['товар', 'товара', 'товаров']);

        return (
            <div className='cart'>
                <Link
                    className='cart__header'
                    href='/order'
                    onClick={this.handleToggleCartBody}
                >
                    <IconCart fill='geyser' />
                    <div className='cart__header-text'>
                        <span className='ui-link ui-link_dashed'>
                            Корзина
                        </span>
                        <div className='cart__info'>
                            {allItems} {productsStr} / {allPrice}&thinsp;₽
                        </div>
                    </div>
                </Link>
                <TooltipPanel
                    collapsed={cartCollapsed}
                    className='cart__body'
                    collapsedClassName='cart__body_collapsed'
                >
                    {isEmpty ?
                        <div className='cart__empty'>
                            Корзина пуста.
                            <br />
                            <br />
                            ¯\_(ツ)_/¯
                            <br />
                            <br />
                            Перейдите
                            {' '}
                            <ReactRouterLink
                                className='ui-link'
                                to='/catalog/'
                                onClick={this.handleCloseCartBody}
                            >
                                в&nbsp;каталог
                            </ReactRouterLink>
                            {' '}
                            и&nbsp;выберите&nbsp;что‑нибудь.
                        </div> :
                        <div>
                            <table className='cart__table'>
                                <tbody className='cart__tbody'>
                                    {models.map((item, i) => <CartRow
                                        key={item.model.id}
                                        title={item.model.title}
                                        amount={item.count}
                                        price={item.model.price.value}
                                        url={getUrlPath(tree, item.model.id)}
                                        onRedirect={this.handleCloseCartBody}
                                        onRemove={() => removeCartItem(item.model.id, store, client)}
                                    />)}
                                    <CartRow total={allPrice} />
                                </tbody>
                            </table>
                            <ReactRouterLink
                                className='ui-button ui-button_size_m'
                                to='/order/#form'
                                onClick={this.handleCloseCartBody}
                            >
                                Оформить заказ
                            </ReactRouterLink>
                        </div>
                    }
                </TooltipPanel>
            </div>
        );
    }
}

// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ order: state.order, tree: state.tree });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Cart);
