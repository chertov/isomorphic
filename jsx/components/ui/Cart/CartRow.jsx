import React, { Component, PropTypes as pt } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { RemoveButton } from '../RemoveButton';

import './ui-cart.less';

class CartRow extends Component {

    static propTypes = {
        title: pt.string,
        amount: pt.number,
        price: pt.number,
        total: pt.number,
        url: pt.string,
        onRedirect: pt.func,
        onRemove: pt.func
    };

    static defaultProps = {
        onRedirect: () => {},
        onRemove: () => {}
    };

    render() {
        const { title, amount, price, total, url, onRedirect, onRemove } = this.props;

        return (
            (total || total === 0) ?
                <tr className='cart__tr'>
                    <td colSpan='3' className='cart__td cart__td_total'>
                        <div className='cart__total'>
                            ИТОГО: {total}&thinsp;₽
                        </div>
                    </td>
                    <td />
                </tr> :
                <tr className='cart__tr'>
                    <td className='cart__td cart__td_name'>
                        <ReactRouterLink
                            className='ui-link'
                            to={url}
                            onClick={onRedirect}
                        >
                            {title}
                        </ReactRouterLink>
                    </td>
                    <td className='cart__td cart__td_amount'>
                        {amount} шт.
                    </td>
                    <td className='cart__td cart__td_price'>
                        {price * amount}&thinsp;₽
                    </td>
                    <td className='cart__td cart__td_remove'>
                        <RemoveButton
                            size={22}
                            title='Убрать из корзины'
                            onClick={onRemove}
                        />
                    </td>
                </tr>
        );
    }
}

export default CartRow;
