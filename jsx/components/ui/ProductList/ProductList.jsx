import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Link as ReactRouterLink } from 'react-router';
import { getUrlPath } from 'tools';
import { Tile } from '../Tile';

import './ui-product-list.less';

class ProductList extends Component {
    static propTypes = {
        tree: pt.object,
        className: pt.string,
        products: pt.array
    };

    renderProduct = product => {
        const { tree } = this.props;
        const baseModel = tree.models[product.models[0]];
        const haveModels = product.models.length > 1;
        let price = baseModel.price.value;

        product.models.forEach(id => {
            const model = tree.models[id];

            if (model.price.value && model.price.value < price) { price = model.price.value; }
        });

        const { title, shortTitle, summary, description } = product;
        let { images } = product;

        if (!images || images.length === 0) { images = ['noimage']; }

        return (
            <Tile
                className='ui-product-list__item'
                key={product.id}
                itemProp='itemListElement'
                itemScope
                itemType='http://schema.org/Product'
            >
                <div className='ui-product-list__item-info'>
                    <ReactRouterLink
                        className='ui-product-list__item-image-wrapper'
                        to={getUrlPath(tree, product.id)}
                        itemProp='url'
                    >
                        <img
                            alt={title}
                            src={`/images/items/${images[0]}.png`}
                            className='ui-product-list__item-image'
                            itemProp='image'
                        />
                    </ReactRouterLink>

                    <div className='ui-product-list__item-title'>
                        <ReactRouterLink
                            className='ui-link'
                            to={getUrlPath(tree, product.id)}
                            itemProp='url'
                        >
                            {shortTitle || title}
                            <meta itemProp='name' content={title || shortTitle} />
                        </ReactRouterLink>
                    </div>

                    <meta itemProp='description' content={description} />

                    <div className='ui-product-list__item-summary' itemProp='disambiguatingDescription'>
                        {summary}
                    </div>
                </div>

                {(price || price === 0)
                    && <div className='ui-product-list__item-buy'>
                        <div
                            className='ui-product-list__item-buy-price'
                            itemProp='offers'
                            itemScope itemType='http://schema.org/Offer'
                        >
                            <div className='ui-product-list__item-price' itemProp='price'>
                                {haveModels ? `От\u00a0${price}` : `${price}`}₽
                            </div>
                        </div>
                    </div>
                }
            </Tile>
        );
    };

    render() {
        const { className, tree } = this.props;
        const products = this.props.products.map(productId => tree.products[productId]);

        return (
            <div
                className={classNames({
                    'ui-product-list': true,
                    [className]: className
                })}
                itemScope itemType='http://schema.org/ItemList'
            >
                { products.map(this.renderProduct) }

                <div className='ui-product-list__empty' />
                <div className='ui-product-list__empty' />
                <div className='ui-product-list__empty' />
            </div>
        );
    }
}

const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(ProductList);
