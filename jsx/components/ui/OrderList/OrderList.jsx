import React, { Component, PropTypes as pt } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { getUrlPath } from 'tools';
import { removeCartItem } from 'api/cart';
import { Tile } from '../Tile';
import { Title } from '../Title';
import { Link } from '../Link';
import { ButtonBuy } from '../ButtonBuy';
import { RemoveButton } from '../RemoveButton';

import './ui-order-list.less';

class OrderList extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        className: pt.string,
        order: pt.object,
        tree: pt.object
    };

    render() {
        const { store, client } = this.context;
        const { className, order, tree } = this.props;

        let totalPrice = 0;

        const models = [];

        order.items.forEach(item => {
            const Model = tree.models[item.modelId];

            models.push({ Model, count: item.count });
            totalPrice += Model.price.value * item.count;
        });

        const isEmpty = !models.length;

        return (
            <Tile>
                {!isEmpty
                    && <Title tag='h3' className='ui-order-list__block-title'>
                        Проверьте свой заказ
                    </Title>
                }

                {isEmpty ?
                    <div className='ui-order-list__empty'>
                        Корзина пуста.
                        <br />
                        <br />
                        ¯\_(ツ)_/¯
                        <br />
                        <br />
                        Перейдите <Link href='/catalog/'>в&nbsp;каталог</Link> и&nbsp;выберите&nbsp;что‑нибудь.
                    </div> :
                    <table
                        className={classNames({
                            'ui-order-list__table': true,
                            [className]: className
                        })}
                    >
                        <tbody className='ui-order-list__tbody'>
                            {models.map((item, i) => {
                                const product = tree.products[item.Model.product];
                                const images = !!item.Model.images && item.Model.images.length > 0 ? item.Model.images : product.images;

                                return (<tr
                                    key={item.Model.id || i}
                                    className={classNames({
                                        'ui-order-list__tr': true,
                                        'ui-order-list__tr_fade': item.Count === 0
                                    })}
                                >
                                    <td className='ui-order-list__td ui-order-list__td_image'>
                                        <img
                                            src={`/images/items/${images[0] || 'noimage'}.png`}
                                            className='ui-order-list__image'
                                            alt={item.Model.title}
                                        />
                                    </td>
                                    <td className='ui-order-list__td ui-order-list__td_title'>
                                        <ReactRouterLink
                                            className='ui-link ui-order-list__title'
                                            to={getUrlPath(tree, item.Model.id)}
                                        >
                                            {item.Model.title}
                                        </ReactRouterLink>
                                    </td>
                                    <td className='ui-order-list__td ui-order-list__td_amount'>
                                        <div className='ui-order-list__amount'>
                                            <ButtonBuy
                                                count={item.count}
                                                allowZero
                                                modelId={item.Model.id}
                                                modelUrl={`/order/${item.Model.id}`}
                                                small
                                                className='ui-order-list__buy-button'
                                            />
                                        </div>
                                    </td>
                                    <td className='ui-order-list__td ui-order-list__td_price'>
                                        <div className='ui-order-list__price'>
                                            {item.Model.price.value * item.count}&thinsp;₽
                                        </div>
                                    </td>
                                    <td className='ui-order-list__td ui-order-list__td_remove'>
                                        <RemoveButton
                                            title='Убрать из заказа'
                                            className='ui-order-list__remove'
                                            href={`/order/${item.Model.id}/rm`}
                                            onClick={() => removeCartItem(item.Model.id, store, client)}
                                        />
                                    </td>
                                </tr>);
                            }
                            )}
                            <tr className='ui-order-list__tr ui-order-list__tr_total'>
                                <td colSpan='4' className='ui-order-list__td ui-order-list__td_total'>
                                    <div className='ui-order-list__total'>
                                        ИТОГО: {totalPrice}&thinsp;₽
                                    </div>
                                </td>
                                <td className='ui-order-list__td' />
                            </tr>
                        </tbody>
                    </table>
                }
            </Tile>
        );
    }
}

// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ order: state.order, tree: state.tree });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(OrderList);
