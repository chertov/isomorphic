import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-columns.less';

class Columns extends Component {

    static propTypes = {
        columns: pt.oneOfType([pt.number, pt.string]),
        mobColumns: pt.oneOfType([pt.number, pt.string]),
        className: pt.string,
        children: pt.node
    };

    static defaultProps = {
        columns: 2,
        mobColumns: 1
    };

    render() {
        const { columns, mobColumns, className, children } = this.props;

        return (
            <div
                className={classNames({
                    'ui-columns': true,
                    [`ui-columns_${columns}`]: columns,
                    [`ui-columns_mob_${mobColumns}`]: mobColumns,
                    [className]: className
                })}
            >
                {children}
            </div>
        );
    }
}

export default Columns;
