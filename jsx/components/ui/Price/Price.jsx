import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-price.less';

class Price extends Component {

    static propTypes = {
        value: pt.oneOfType([pt.number, pt.string]),
        className: pt.string
    };

    render() {
        const { value, className } = this.props;

        if (isNaN(value)) {
            return null;
        }

        return (
            <div
                className={classNames({
                    'ui-price': true,
                    [className]: className
                })}
                itemProp='offers'
                itemScope itemType='http://schema.org/Offer'
            >
                <span className='ui-price__value' itemProp='price'>{value}</span>
                <span className='ui-price__symbol'>&thinsp;<span itemProp='priceCurrency' content='RUB'>₽</span></span>
            </div>
        );
    }
}

export default Price;
