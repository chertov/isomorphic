import React from 'react';
import { Icon } from '../';

const IconClock = props =>

    <Icon {...props} viewBox='0 0 100 100'>
        <path d='M51.5,5.5c-24.262,0-44,19.738-44,44s19.738,44,44,44s44-19.738,44-44S75.762,5.5,51.5,5.5z M51.5,85.5  c-19.852,0-36-16.15-36-36s16.148-36,36-36c19.85,0,36,16.15,36,36S71.35,85.5,51.5,85.5z M55.5,21.5h-8v29.656l16.172,16.172  l5.656-5.656L55.5,47.844V21.5z' />
    </Icon>;

export default IconClock;
