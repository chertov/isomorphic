import React from 'react';
import { Icon } from '../';

const IconExternalLink = props =>

    <Icon {...props} viewBox='0 0 100 100'>
        <path strokeWidth='10' d='m43,35H5v60h60V57M45,5v10l10,10-30,30 20,20 30-30 10,10h10V5z' />
    </Icon>;

export default IconExternalLink;
