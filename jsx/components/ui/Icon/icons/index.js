import IconExternalLink from './IconExternalLink.jsx';
import IconComission from './IconComission.jsx';
import IconCart from './IconCart.jsx';
import IconClose from './IconClose.jsx';
import IconClock from './IconClock.jsx';
import IconPackage from './IconPackage.jsx';
import IconQuestion from './IconQuestion.jsx';
import IconSMS from './IconSMS.jsx';
import IconWallet from './IconWallet.jsx';
import IconWarning from './IconWarning.jsx';

export {
    IconExternalLink,
    IconComission,
    IconCart,
    IconClose,
    IconClock,
    IconPackage,
    IconQuestion,
    IconSMS,
    IconWallet,
    IconWarning
};
