import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-icon.less';

class Icon extends Component {

    static propTypes = {
        viewBox: pt.string,
        strokeWidth: pt.string,
        className: pt.string,
        style: pt.object,
        svgClassName: pt.string,
        size: pt.oneOfType([pt.number, pt.string]),
        fill: pt.string,
        stroke: pt.string,
        mobileSize: pt.number,
        isButton: pt.bool,
        title: pt.string,
        onClick: pt.func,
        children: pt.node
    };

    static defaultProps = {
        size: 32,
        viewBox: '0 0 100 100',
        strokeWidth: '0',
        onClick: () => {}
    };

    render() {
        const { viewBox, strokeWidth, size, fill, stroke, mobileSize, className, style, svgClassName, isButton, title, onClick } = this.props,
            Tag = isButton ? 'button' : 'span';

        return <Tag
            className={classNames({
                'ui-icon': true,
                'ui-icon_button': isButton,
                [`ui-icon_size_${size}`]: size,
                [`ui-icon_size_mobile_${mobileSize}`]: mobileSize,
                [className]: className
            })}
            style={style}
            onClick={onClick}
            title={title}
        >
            <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox={viewBox}
                strokeWidth={strokeWidth}
                className={classNames({
                    'ui-icon__svg': true,
                    [`ui-icon__svg_fill_${fill}`]: fill,
                    [`ui-icon__svg_stroke_${stroke}`]: stroke,
                    [svgClassName]: svgClassName
                })}
            >
                {(fill === 'gradient' || stroke === 'gradient')
                    && <defs>
                        <linearGradient id='iconGradient'>
                            <stop offset='0%' stopColor='#5F5370' />
                            <stop offset='100%' stopColor='#877D99' />
                        </linearGradient>
                    </defs>
                }
                {this.props.children}
            </svg>
        </Tag>;
    }
}

export default Icon;
