import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Link as ReactRouterLink } from 'react-router';
import { Tile } from '../Tile';

import './ui-onlays-showcase.less';

class OnlaysShowcase extends Component {

    static propTypes = {
        tree: pt.object,
        className: pt.string
    };

    renderProduct = productName => {
        const { tree } = this.props;
        const product = tree.products[productName];

        if (!product) { return null; }

        const { images = [], title, shortTitle, summary, models } = product;

        let price = tree.models[models[0]].price.value;

        models.forEach(id => {
            const model = tree.models[id];

            if (model.price.value && model.price.value < price) { price = model.price.value; }
        });

        return <div className='ui-onlays-showcase__item'>
            <ReactRouterLink
                className='ui-onlays-showcase__item-image-wrapper'
                to={`/catalog/onlays/common/${product.id}`}
            >
                <img
                    alt={title}
                    src={`/images/items/${images[0]}.png`}
                    className='ui-onlays-showcase__item-image'
                />
            </ReactRouterLink>
            <div className='ui-onlays-showcase__item-title'>
                <ReactRouterLink
                    className='ui-link'
                    to={`/catalog/onlays/common/${product.id}`}
                >
                    {shortTitle || title}
                </ReactRouterLink>
            </div>
            <div className='ui-onlays-showcase__item-summary'>
                {summary}
            </div>
            <div className='ui-onlays-showcase__item-price'>
                {(models.length > 1) ? `От\u00a0${price}` : `${price}`}₽
            </div>
        </div>;
    };

    render() {
        const { className } = this.props;

        return (
            <Tile
                className={classNames({
                    'ui-onlays-showcase': true,
                    [className]: className
                })}
            >
                <div
                    className={classNames({
                        'ui-onlays-showcase__column': true,
                        'ui-onlays-showcase__column_1': true
                    })}
                >
                    {this.renderProduct('office_vip')}
                </div>
                <div
                    className={classNames({
                        'ui-onlays-showcase__column': true,
                        'ui-onlays-showcase__column_2': true
                    })}
                >
                    <div className='ui-onlays-showcase__row'>
                        {this.renderProduct('office')}
                        {this.renderProduct('office_children')}
                    </div>

                    <div className='ui-onlays-showcase__row'>
                        <div className='ui-onlays-showcase__item'>
                            {/* <img
                                src='/images/salt.jpg'
                                alt='Бонус'
                                className='ui-onlays-showcase__bonus-image'
                            />
                            <div className='ui-onlays-showcase__bonus-dicount'>−15%</div>
                            <div>Всем офисом — дешевле.</div>
                            Скидка при заказе от 4 офисных накладок */}
                        </div>
                    </div>
                </div>
            </Tile>
        );
    }
}

const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(OnlaysShowcase);
