import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import { Link as ReactRouterLink } from 'react-router';
import md5 from 'blueimp-md5';

import { loginMe, logoutMe, newPasswordMe, LOGIN_ERROR } from 'api/me';
import { LOGIN_LOADER } from 'reducers/me';
import { Link } from '../Link';
import { Field } from '../Field';
import { Button } from '../Button';
import { TooltipPanel } from '../TooltipPanel';
import { Loading } from '../Loading';
import { getErrorMsg } from '../../../constants/errorMessages';

import './ui-auth.less';

class Auth extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };
    static propTypes = {
        me: pt.object,
        errors: pt.object,
        activeLoaders: pt.object
    };

    state = { authCollapsed: true };

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.activeLoaders[LOGIN_LOADER] !== nextProps.activeLoaders[LOGIN_LOADER]
            && !nextProps.activeLoaders[LOGIN_LOADER]) { // детектим завершение лоадера логина
            if (this.props.errors[LOGIN_ERROR] && this.props.errors[LOGIN_ERROR].name === '') { // если логин закончен и не было ошибок
                this.setState({ authCollapsed: true });
            }
        }
    }

    handleClickOutside = event => {
        if (this.state.authCollapsed) { return; }
        const domNode = ReactDOM.findDOMNode(this);

        if ((!domNode || !domNode.contains(event.target))) {
            this.setState({ authCollapsed: true });
        }
    };

    handleToggleAuth = event => {
        event.preventDefault();
        this.setState({
            authCollapsed: !this.state.authCollapsed,
            isForgot: !this.state.authCollapsed
        });
    };

    handleCloseCartBody = event => {
        this.setState({ authCollapsed: true });
    };

    handleForgot = event => {
        event.preventDefault();
        this.setState({
            isForgot: !this.state.isForgot
        });
    };

    handleSubmit = event => {
        event.preventDefault();
        const { store, client } = this.context;
        const { phone, password, isForgot } = this.state;

        if (!isForgot) {
            loginMe(phone, md5(phone + password), store, client, () => {
                this.setState({ authCollapsed: true });
            });
        } else {
            newPasswordMe(phone, store, client, () => {
                this.setState({ isForgot: false });
            });
        }
    };

    handleOnLogout = event => {
        event.preventDefault();
        const { store, client } = this.context;

        logoutMe(store, client);
        this.setState({ authCollapsed: true });
    };

    handleChangePhone = value => {
        this.setState({ phone: value });
    };

    handleChangePassword = value => {
        this.setState({ password: value });
    };

    renderLoginForm = () => {
        const errors = this.props.errors[LOGIN_ERROR] || [];
        const { phone, password, isForgot } = this.state;

        return <form
            action={isForgot ? '/api/newpassword' : '/api/login'}
            method='post'
            onSubmit={this.handleSubmit}
        >
            <Field
                id='login_phone'
                label='Телефон'
                type='tel'
                value={phone}
                onChange={this.handleChangePhone}
                mask='+79999999999'
                error={getErrorMsg(errors, ['USER_NOT_FOUND', 'PHONE_NOT_FOUND', 'PHONE_ISNT_VALID'])}
            />
            {isForgot ?
                <div className='auth__forgot-hint'>
                    Мы вышлем новый пароль на&nbsp;ваш номер телефона
                </div> :
                <Field
                    id='login_password'
                    label='Пароль'
                    type='password'
                    value={password}
                    onChange={this.handleChangePassword}
                    error={getErrorMsg(errors, 'USER_PASSWORD_INCORRECT')}
                />
            }
            <div className='auth__buttons'>
                <Button
                    type='submit'
                    title={isForgot ? 'Выслать в СМС' : 'Войти'}
                />
                {!isForgot
                    && <Link
                        dashed
                        className='auth__forgot-link'
                        onClick={this.handleForgot}
                        href='/login/newpassword'
                    >
                        Не помню пароль
                    </Link>
                }
            </div>

            <Loading coverParent id={LOGIN_LOADER} />
        </form>;
    };

    render() {
        const { authCollapsed } = this.state;
        const { me } = this.props;
        const profileTitle = 'Личный кабинет'; // me.name.length > 10 ? 'Кабинет' : me.name;

        return (
            <span className='auth'>
                <Link
                    dashed
                    onClick={this.handleToggleAuth}
                    href={me ? '/profile' : '/login'}
                >
                    {me ? profileTitle : 'Вход с паролем'}
                </Link>

                <TooltipPanel
                    collapsed={authCollapsed}
                    className='auth__block'
                >
                    {!me ?
                        this.renderLoginForm() :
                        <div className='auth__profile-links'>
                            {/* <div className='auth__profile-link'>
                                <ReactRouterLink
                                    to='/profile/edit'
                                    onClick={this.handleCloseCartBody}
                                    className='ui-link'
                                >
                                    Перейти в кабинет
                                </ReactRouterLink>
                            </div>
                            <div className='auth__profile-link'>
                                <ReactRouterLink
                                    to='/profile/edit'
                                    onClick={this.handleCloseCartBody}
                                    className='ui-link'
                                >
                                    Изменить телефон, адрес
                                </ReactRouterLink>
                            </div>*/}
                            <div className='auth__profile-link'>
                                <ReactRouterLink
                                    to='/profile/orders'
                                    onClick={this.handleCloseCartBody}
                                    className='ui-link'
                                >
                                    История заказов
                                </ReactRouterLink>
                            </div>
                            {/* <div className='auth__profile-link'>
                                <ReactRouterLink
                                    to='/profile/bonuses'
                                    onClick={this.handleCloseCartBody}
                                    className='ui-link'
                                >
                                    Мои скидки
                                </ReactRouterLink>
                            </div>*/}
                            <div className='auth__profile-link'>
                                <ReactRouterLink
                                    to='/profile/logout'
                                    onClick={this.handleOnLogout}
                                    className='ui-link ui-link_dashed'
                                >
                                    Выход
                                </ReactRouterLink>
                            </div>
                        </div>
                    }
                </TooltipPanel>
            </span>
        );
    }
}


// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ me: state.me, errors: state.errors, activeLoaders: state.activeLoaders });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Auth);
