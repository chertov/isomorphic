import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-tooltip-panel.less';

class TooltipPanel extends Component {

    static propTypes = {
        className: pt.string,
        children: pt.node,
        collapsed: pt.bool
    };

    render() {
        const { className, children, collapsed } = this.props;

        return (
            <div
                className={classNames({
                    'ui-tooltip-panel': true,
                    'ui-tooltip-panel_collapsed': collapsed,
                    [className]: className
                })}
            >
                {children}
            </div>
        );
    }
}

export default TooltipPanel;
