import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-button.less';

class Button extends Component {
    static propTypes = {
        title: pt.string,
        size: pt.string,
        disabled: pt.bool,
        loading: pt.bool,
        type: pt.string,
        href: pt.string,
        onClick: pt.func,
        className: pt.string,
        children: pt.node
    };

    static defaultProps = {
        size: 'm',
        href: '',
        type: 'button'
    };

    handleClick = event => {
        if (this.props.disabled) {
            event.preventDefault();
            return;
        }

        if (this.props.onClick) {
            event.preventDefault();
            this.buttonRef.blur();
            this.props.onClick(event);
        }
    };

    render() {
        const { title, size, disabled, loading, type, href, className, children } = this.props;
        const Tag = (href && !disabled) ? 'a' : 'button';

        const button = <Tag
            ref={ref => { this.buttonRef = ref; }}
            role='button'
            type={href ? '' : type}
            href={href}
            disabled={disabled || loading}
            onClick={this.handleClick}
            className={classNames({
                'ui-button': true,
                'ui-button_loading': loading,
                'ui-button_disabled': disabled,
                [`ui-button_size_${size}`]: size,
                [className]: className
            })}
            title={title}
        >
            <span
                className={classNames({
                    'ui-button__content': true,
                    'ui-button__content_loading': loading
                })}
            >
                {children || title}
            </span>
            <div
                className={classNames({
                    'ui-button__loader': true,
                    'ui-button__loader_loading': loading
                })}
            />
        </Tag>;

        return button;
    }
}

export default Button;
