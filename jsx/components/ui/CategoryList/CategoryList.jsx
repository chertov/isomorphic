import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Link as ReactRouterLink } from 'react-router';
import { Tile } from '../Tile';

import './ui-category-list.less';

class CategoryList extends Component {
    static contextTypes = {
        urlPath: pt.string
    }
    static propTypes = {
        tree: pt.object,
        folder: pt.bool,
        className: pt.string,
        items: pt.array
    };

    static defaultProps = {
        items: []
    };

    render() {
        const { className, folder, tree } = this.props;
        const { urlPath } = this.context;
        const items = this.props.items.map(item => (folder ? tree.folders[item] : tree.categories[item]));

        return (
            <div
                className={classNames({
                    'ui-category-list': true,
                    [className]: className
                })}
            >
                {
                    items.map((item, i) => (
                        <Tile
                            key={item.id || i}
                            className={classNames({
                                'ui-category-list__item': true,
                                [className]: className
                            })}
                        >
                            <div className='ui-category-list__item-top'>
                                <div className='ui-category-list__item-title'>
                                    <ReactRouterLink
                                        className='ui-link'
                                        to={`${urlPath}${item.id}`}
                                    >
                                        {item.title}
                                    </ReactRouterLink>
                                </div>
                            </div>

                            <div className='ui-category-list__item-bottom'>
                                {item.products
                                    && <div className='ui-category-list__item-info'>
                                        Товаров: {item.products.length}
                                    </div>
                                }
                            </div>
                        </Tile>
                    ))
                }

                {/* additional items to keep grid-view of flexbox */}
                <div className='ui-category-list__empty' />
                <div className='ui-category-list__empty' />
            </div>
        );
    }
}

const mapStateToProps = state => ({ tree: state.tree });

export default connect(mapStateToProps)(CategoryList);
