import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-input.less';

class Input extends Component {

    static propTypes = {
        id: pt.string,
        value: pt.oneOfType([pt.number, pt.string]),
        type: pt.string,
        pattern: pt.string,
        inputmode: pt.string,
        min: pt.string,
        max: pt.string,
        autoFocus: pt.bool,
        onChange: pt.func,
        wide: pt.bool,
        phoneWide: pt.bool,
        className: pt.string,
        error: pt.bool
    };

    static defaultProps = {
        type: 'text',
        phoneWide: true,
        onChange: () => {}
    };

    componentDidMount = () => {
        this.props.autoFocus
            && this.inputRef.focus();
    };

    handleChange = event => {
        this.props.onChange(event.target.value);
    };

    render() {
        const { id, value, type, pattern, inputmode, min, max, wide, phoneWide, error, className } = this.props;

        return (
            <input
                id={id}
                ref={ref => { this.inputRef = ref; }}
                type={type}
                pattern={pattern}
                inputMode={inputmode}
                min={min}
                max={max}
                value={value}
                onChange={this.handleChange}
                className={classNames({
                    'ui-input': true,
                    'ui-input_wide': wide,
                    'ui-input_phone-wide': phoneWide,
                    [`ui-input_type_${type}`]: type,
                    'ui-input_error': error,
                    [className]: className
                })}
            />
        );
    }
}

export default Input;
