import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';
import { IconExternalLink } from '../Icon/icons';

import './ui-link.less';

class Link extends Component {

    static propTypes = {
        className: pt.string,
        external: pt.bool,
        addExternalIcon: pt.bool,
        dashed: pt.bool,
        href: pt.string,
        title: pt.string,
        onClick: pt.func,
        children: pt.node,
        itemProp: pt.string // schema.org
    };

    static defaultProps = {
        onClick: () => {}
    };

    render() {
        const { className, external, addExternalIcon, dashed, href, title, onClick, children, itemProp } = this.props;

        return (
            <a
                className={classNames({
                    'ui-link': true,
                    'ui-link_dashed': dashed,
                    [className]: className
                })}
                href={href}
                target={external && '_blank'}
                title={title || (external && 'Внешняя ссылка.\nОткроется в новой вкладке.')}
                onClick={onClick}
                itemProp={itemProp}
            >
                {children}
                {addExternalIcon
                    && <IconExternalLink
                        className='ui-link__external-icon'
                        style={{
                            verticalAlign: 'baseline',
                            width: '0.9em',
                            marginLeft: '0.3em'
                        }}
                    />
                }
            </a>
        );
    }
}

export default Link;
