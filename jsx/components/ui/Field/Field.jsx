import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';
import { Input } from '../Input';
import { mask } from '../../../../utils/mask';

import './ui-field.less';

class Field extends Component {

    static propTypes = {
        id: pt.string,
        label: pt.string,
        value: pt.oneOfType([pt.string, pt.number]),
        fieldClassName: pt.string,
        wide: pt.bool,
        phoneWide: pt.bool,
        onChange: pt.func,
        onBlur: pt.func,
        error: pt.string,
        mask: pt.string,
        className: pt.string
    };

    static defaultProps = {
        onChange: () => {},
        onBlur: () => {},
        value: ''
    };

    state = {
        isEmpty: !`${this.props.value}`,
        isFocused: false,
        showError: false,
        unmaskedValue: false
    };

    handleChange = value => {
        this.setState({
            isEmpty: !`${value}`,
            showError: false
        });

        const validatedValue = this.validate(value, this.props.mask);

        this.props.onChange(validatedValue);
    };

    handleFocus = event => {
        this.setState({
            isFocused: true
        });

        if (this.props.value === '') {  // если поле пустое, то
            this.handleChange('');      // дёргаем handleChange чтобы сразу отобразить маску
        }                               // и заодно сбросить showError, который мог быть после блюра
    };

    handleBlur = event => {
        this.setState({
            isFocused: false,
            showError: true
        });

        if (this.state.unmaskedValue === '') {  // если пользовательского ввода нету, то
            this.props.onChange('');            // очищаем поле, чтоб не осталось пустой маски
            this.setState({
                isEmpty: true                   // ну и устанавливаем isEmpty
            });
        }

        this.props.onBlur();
    };

    validate = (value, myMask) => {
        if (myMask) {
            const masked = mask(value, myMask);
            // const masked = mask(value, '+7 (999) 999-99-99'); // косяк с удалением бэкспейсом

            console.log('----------------');
            console.log(masked);

            this.setState({
                unmaskedValue: masked.unmaskedValue
            });

            return masked.maskedValue;
        }

        return value;
    };

    render() {
        const { id, label, fieldClassName, wide, phoneWide, error, className } = this.props;
        const { isEmpty, isFocused, showError } = this.state;

        return (
            <div
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                className={classNames({
                    'ui-field': true,
                    [className]: className
                })}
            >
                <div
                    className={classNames({
                        'ui-field__input-wrapper': true,
                        'ui-field__input-wrapper_wide': wide,
                        'ui-field__input-wrapper_phone-wide': phoneWide
                    })}
                >
                    <label
                        className={classNames({
                            'ui-field__label': true,
                            'ui-field__label_up': !isEmpty || isFocused,
                            'ui-field__label_error': !!error,
                            'ui-field__label_focused': isFocused
                        })}
                        htmlFor={id}
                    >
                        {label}
                    </label>
                    <Input
                        {...this.props}
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        error={!!error}
                        className={fieldClassName}
                    />
                </div>
                <div
                    className={classNames({
                        'ui-field__error': true,
                        'ui-field__error_collapse': !showError || !error
                    })}
                >
                    <div className='ui-field__error-text'>
                        {error || '\u00a0'}
                    </div>
                </div>
            </div>
        );
    }
}

export default Field;
