import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Container } from '../Container';
import { IconClose, IconWarning } from '../Icon/icons';

import { hideError } from './../../../reducers/error';

import './ui-alert.less';

class Alert extends Component {

    static propTypes = {
        color: pt.string,
        className: pt.string,
        error: pt.shape({
            show: pt.bool.isRequired,
            text: pt.string.isRequired
        }).isRequired
    };

    static defaultProps = {
        error: {
            show: false,
            text: ''
        }
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.error.show === nextProps.error.show
            && this.props.error.show === false) {
            return false;
        }
        if (this.props.error.show === nextProps.error.show
            && this.props.error.text === nextProps.error.text) {
            return false;
        }
        return true;
    }

    render = () => {
        const { color, className } = this.props;
        const { show, text } = this.props.error;

        return (
            <div
                className={classNames({
                    'ui-alert': true,
                    'ui-alert_collapsed': !show,
                    [`ui-alert_${color}`]: color,
                    [className]: className
                })}
            >
                <Container>
                    <div className='ui-alert__container'>
                        <IconWarning
                            fill='white'
                            stroke='white'
                            className='ui-alert__icon'
                        />
                        <span className='ui-alert__text'>
                            {text}
                        </span>
                        <IconClose
                            onClick={hideError}
                            isButton
                            title='Закрыть сообщение об ошибке'
                            size='24'
                            fill='white'
                            stroke='white'
                            className='ui-alert__close'
                        />
                    </div>
                </Container>
            </div>
        );
    }
}

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ error: state.error });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Alert);
