import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-title.less';

class Title extends Component {

    static propTypes = {
        className: pt.string,
        id: pt.string,
        tag: pt.string,
        gradient: pt.bool,
        children: pt.node
    };

    static defaultProps = {
        tag: 'div'
    };

    render() {
        const { className, id, tag, gradient, children } = this.props;
        const Tag = tag;

        return (
            <Tag
                id={id}
                className={classNames({
                    'ui-title': true,
                    [`ui-title_${tag}`]: tag,
                    [className]: className
                })}
            >
                <span
                    className={classNames({
                        'ui-title__gradient': gradient
                    })}
                >
                    {children}
                </span>
            </Tag>
        );
    }
}

export default Title;
