import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';
import { Input } from '../Input';

import './ui-select.less';

class Select extends Component {

    static propTypes = {
        options: pt.array,
        canInputInstead: pt.bool,
        value: pt.number,
        className: pt.string,
        optionClassName: pt.string,
        onChange: pt.func
    };

    static defaultProps = {
        options: [],
        onChange: () => {}
    };

    handleChange = event => {
        this.props.onChange(parseInt(event.target.value, 10));
    };

    render() {
        const { options, className, optionClassName, canInputInstead, value } = this.props;
        const showInputInstead = value > options.length;

        return (
            showInputInstead ?
                <Input
                    value={value}
                    type='number'
                    pattern='[0-9]*'
                    inputmode='numeric'
                    min='0'
                    max='99'
                    autoFocus
                    onChange={this.handleChange}
                /> :
                <select
                    value={value}
                    onChange={this.handleChange}
                    className={classNames({
                        'ui-select': true,
                        [className]: className
                    })}
                >
                    {options.map((option, i) => (
                        <option
                            key={option.value || i}
                            value={option.value}
                            className={classNames({
                                'ui-select__option': true,
                                [optionClassName]: optionClassName
                            })}
                        >
                            {option.title}
                        </option>
                    ))}
                    {canInputInstead
                        && <option
                            value='showInputInstead'
                            className={classNames({
                                'ui-select__option': true,
                                [optionClassName]: optionClassName
                            })}
                        >
                            Ввести самому
                        </option>
                    }
                </select>
        );
    }
}

export default Select;
