import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-checkbox.less';

class Checkbox extends Component {

    static propTypes = {
        id: pt.string,
        value: pt.number,
        checked: pt.bool,
        label: pt.string,
        onChange: pt.func,
        className: pt.string
    };

    static defaultProps = {
        onChange: () => {}
    };

    handleChange = event => {
        this.props.onChange(event.target.checked);
    };

    render() {
        const { id, value, checked, label, className } = this.props;

        return (
            <div
                className={classNames({
                    'ui-checkbox': true,
                    [className]: className
                })}
            >
                <input
                    id={id}
                    type='checkbox'
                    value={value}
                    checked={checked}
                    onChange={this.handleChange}
                    className='ui-checkbox__input'
                />
                <label
                    htmlFor={id}
                    className='ui-checkbox__label'
                >
                    <div className='ui-checkbox__box' />
                    {label}
                </label>
            </div>
        );
    }
}

export default Checkbox;
