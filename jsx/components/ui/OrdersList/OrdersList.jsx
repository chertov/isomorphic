import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Tile } from '../Tile';

import './ui-orders-list.less';

class OrdersList extends Component {
    static propTypes = {
        className: pt.string,
        orders: pt.array,
        tree: pt.object
    };

    static defaultProps = {
        orders: []
    };

    renderOrder = order => {
        const { className, tree } = this.props;

        let totalPrice = 0;

        const models = [];

        order.items.forEach(item => {
            const Model = tree.models[item.modelId];

            models.push({ Model, count: item.count });
            totalPrice += Model.price.value * item.count;
        });

        if (!models.length) {
            return <Tile>Моделей нет. Пустой заказ. По идее такого быть не может.</Tile>;
        }

        return (
            <Tile className='ui-orders-list__tile' key={order.id}>
                <div className='ui-orders-list__order-title'>
                    Заказ №{order.number}
                </div>
                <div className='ui-orders-list__order-date'>
                    01.04.17
                </div>

                <table
                    className={classNames({
                        'ui-orders-list__table': true,
                        [className]: className
                    })}
                >
                    <tbody className='ui-orders-list__tbody'>
                        {models.map((item, i) => {
                            const product = tree.products[item.Model.product];
                            const images = !!item.Model.images && item.Model.images.length > 0 ? item.Model.images : product.images;

                            return (<tr
                                key={item.Model.id || i}
                                className={classNames({
                                    'ui-orders-list__tr': true,
                                    'ui-orders-list__tr_fade': item.Count === 0
                                })}
                            >
                                <td className='ui-orders-list__td ui-orders-list__td_image'>
                                    <img
                                        src={`/images/items/${images[0] || 'noimage'}.png`}
                                        className='ui-orders-list__image'
                                        alt={item.Model.title}
                                    />
                                </td>
                                <td className='ui-orders-list__td ui-orders-list__td_title'>
                                    <div className='ui-orders-list__title'>
                                        {item.Model.title}
                                    </div>
                                </td>
                                <td className='ui-orders-list__td ui-orders-list__td_amount'>
                                    <div className='ui-orders-list__amount'>
                                        {item.count}
                                    </div>
                                </td>
                                <td className='ui-orders-list__td ui-orders-list__td_price'>
                                    <div className='ui-orders-list__price'>
                                        {item.Model.price.value}&thinsp;₽
                                    </div>
                                </td>
                                <td className='ui-orders-list__td ui-orders-list__td_sumprice'>
                                    <div className='ui-orders-list__sumprice'>
                                        {item.Model.price.value * item.count}&thinsp;₽
                                    </div>
                                </td>
                            </tr>);
                        }
                    )}
                        <tr className='ui-orders-list__tr ui-orders-list__tr_total'>
                            <td colSpan='5' className='ui-orders-list__td ui-orders-list__td_total'>
                                <div className='ui-orders-list__total'>
                                    ИТОГО: {totalPrice}&thinsp;₽
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </Tile>
        );
    };

    render() {
        const orders = this.props.orders ? Object.assign([], this.props.orders) : [];

        return (orders && orders.length) ?
            <div>{orders.sort((order1, order2) => order2.number - order1.number).map(this.renderOrder)}</div> :
            <div>У Вас нет еще ни одного заказа</div>;
    }
}


// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ tree: state.tree });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(OrdersList);
