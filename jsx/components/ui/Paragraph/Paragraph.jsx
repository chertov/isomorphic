import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-paragraph.less';

class Paragraph extends Component {

    static propTypes = {
        hyphenated: pt.bool,
        className: pt.string,
        children: pt.node,
        itemProp: pt.string // schema.org
    };

    doHyphenate = text => {
        const all = '[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]',
            glas = '[аеёиоуыэюя]',
            sogl = '[бвгджзклмнпрстфхцчшщ]',
            zn = '[йъь]',
            shy = '\xAD',
            re = [];

        re[1] = new RegExp(`(${zn})(${all}${all})`, 'ig');
        re[2] = new RegExp(`(${glas})(${glas}${all})`, 'ig');
        re[3] = new RegExp(`(${glas}${sogl})(${sogl}${glas})`, 'ig');
        re[4] = new RegExp(`(${sogl}${glas})(${sogl}${glas})`, 'ig');
        re[5] = new RegExp(`(${glas}${sogl})(${sogl}${sogl}${glas})`, 'ig');
        re[6] = new RegExp(`(${glas}${sogl}${sogl})(${sogl}${sogl}${glas})`, 'ig');

        let processedText = text;

        for (let i = 1; i < 7; ++i) {
            processedText = processedText.replace(re[i], `$1${shy}$2`);
        }

        return processedText;
    };

    render() {
        const { hyphenated, className, children, itemProp } = this.props;

        let hyphenatedText;

        if (hyphenated && typeof children === 'string') {
            hyphenatedText = this.doHyphenate(children);
        }

        return (
            <p
                className={classNames({
                    'ui-paragraph': true,
                    'ui-paragraph_hyphenated': hyphenated,
                    [className]: className
                })}
                itemProp={itemProp}
            >
                {hyphenatedText || children}
            </p>
        );
    }
}

export default Paragraph;
