import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './ui-loading.less';

class Loading extends Component {
    static propTypes = {
        activeLoaders: pt.object,
        id: pt.string,
        coverParent: pt.bool,
        className: pt.string,
        svgClassName: pt.string
    };

    render() {
        const { activeLoaders, id, coverParent, className, svgClassName } = this.props;
        const show = activeLoaders[id];

        return (
            <div
                className={classNames({
                    'ui-loading': true,
                    'ui-loading_cover-parent': coverParent,
                    'ui-loading_hidden': !show,
                    [className]: className
                })}
            >
                <svg
                    xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 50 50'
                    className={classNames({
                        'ui-loading__svg': true,
                        'ui-loading__svg_cover-parent': coverParent,
                        [svgClassName]: svgClassName
                    })}
                >
                    {/* <defs>
                        <linearGradient id='SpinnerGradient' x1='0' x2='1' y1='0' y2='0'>
                            <stop offset='0%' stopColor='#BABAD6' />
                            <stop offset='100%' stopColor='#877D99' />
                        </linearGradient>
                    </defs> */}
                    <path /* fill='url(#SpinnerGradient)' */ d='M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z'>
                        <animateTransform
                            attributeType='xml'
                            attributeName='transform'
                            type='rotate'
                            from='0 25 25'
                            to='360 25 25'
                            dur='0.6s'
                            repeatCount='indefinite'
                        />
                    </path>
                </svg>
            </div>
        );
    }
}
// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ activeLoaders: state.activeLoaders });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Loading);
