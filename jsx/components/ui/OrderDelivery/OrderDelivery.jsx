import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { values } from 'ramda';
import { changeOrder } from 'api/order';

import classNames from 'classnames';
import { Tile } from '../../ui/Tile';
import { Title } from '../../ui/Title';

import './ui-order-delivery.less';

import DELIVERY_TYPES from '../../../constants/deliveryTypes';


class OrderDelivery extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object
    };

    render() {
        const { store, client } = this.context;
        const order = JSON.parse(JSON.stringify(this.props.order));
        const deliveryType = 'pochta-rossii'; // order.delivery.type; // TODO: set dynamically

        return (
            <Tile>
                <Title tag='h3' className='ui-order-delivery__block-title'>
                    Способ доставки {/* TODO: Выберите способ доставки */}
                </Title>
                <div className='ui-order-delivery__options'>
                    {
                        (values(DELIVERY_TYPES)).map((delivery, i) => {
                            const isChecked = delivery.value === deliveryType;

                            return <div
                                key={delivery.value || i}
                                className={classNames({
                                    'ui-order-delivery__option': true,
                                    'ui-order-delivery__option_fade': deliveryType && !isChecked
                                })}
                            >
                                <input
                                    id={`order-delivery_${delivery.value}`}
                                    type='radio'
                                    name='delivery'
                                    value={delivery.value}
                                    checked={isChecked}
                                    className='ui-order-delivery__input'
                                    onChange={event => {
                                        const type = event.target.value;

                                        order.delivery.type = type;
                                        changeOrder(order, store, client);
                                    }}
                                />
                                <label
                                    htmlFor={`order-delivery_${delivery.value}`}
                                    className='ui-order-delivery__label'
                                >
                                    <div
                                        className={classNames({
                                            'ui-order-delivery__radio': true,
                                            'ui-order-delivery__radio_checked': isChecked
                                        })}
                                    />
                                    {delivery.image ?
                                        <img
                                            alt={delivery.title}
                                            src={delivery.image}
                                            className='ui-order-delivery__image'
                                        /> :
                                        <div className='ui-order-delivery__image-empty' />
                                    }
                                    <div className='ui-order-delivery__title'>
                                        {delivery.title}
                                    </div>
                                    <div className='ui-order-delivery__description'>
                                        {delivery.description}
                                    </div>
                                </label>
                            </div>;
                        })
                    }
                </div>
            </Tile>
        );
    }
}

// -----------------------------------------------------

// мапим состяние стора на props компонента
const mapStateToProps = state => ({ order: state.order });

// экспортируем компонент с замаппеным состянием и экшенами
export default connect(mapStateToProps)(OrderDelivery);
