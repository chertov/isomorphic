import React, { Component, PropTypes as pt } from 'react';
import { changeOrder, ERROR_ORDER } from 'api/order';
import { connect } from 'react-redux';
import { Field } from '../Field';
import { getErrorMsg } from '../../../constants/errorMessages';

import './ui-order-form.less';

class OrderFormPR extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object,
        errors: pt.object
    };

    render() {
        const { store, client } = this.context;
        const order = JSON.parse(JSON.stringify(this.props.order));
        const errors = this.props.errors[ERROR_ORDER] || [];

        return (
            <div>
                <form className='ui-order-form__form'>
                    <Field
                        id='pr_phone'
                        label='Ваш телефон'
                        type='tel'
                        value={order.phone}
                        onChange={phone => {
                            order.phone = phone;
                            changeOrder(order, store, client);
                        }}
                        mask='+79999999999'
                        error={getErrorMsg(errors, ['PHONE_ISNT_VALID', 'PHONE_EXISTS_USER_ISNOT_AUTH', 'PHONE_BELONGS_TO_ANOTHER_USER'])}
                    />
                    <Field
                        id='pr_fio'
                        label='ФИО получателя'
                        value={order.fio}
                        onChange={fio => {
                            order.fio = fio;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='pr_address'
                        label='Адрес доставки'
                        wide
                        value={order.delivery.address}
                        onChange={address => {
                            order.delivery.address = address;
                            changeOrder(order, store, client);
                        }}
                    />
                    {/* <Field
                        id='pers_passport'
                        label='Серия и номер паспорта'
                        value={order.fizik.passport}
                        onChange={passport => changeOrder(Object.assign(order, { fizik: { passport } }), store, client)}
                    /> */}
                </form>
                <div className='ui-order-form__hint'>
                    На указанный телефон мы&nbsp;пришлём СМС с&nbsp;паролем для подтверждения заказа.
                    <br />
                    А&nbsp;когда посылка прибудет в&nbsp;пункт выдачи&nbsp;— оповестим о&nbsp;доставке.
                </div>
            </div>
        );
    }
}

// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ order: state.order, errors: state.errors });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(OrderFormPR);
