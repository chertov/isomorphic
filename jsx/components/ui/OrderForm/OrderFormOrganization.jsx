import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { changeOrder, ERROR_ORDER } from 'api/order';
import { Field } from '../Field';
import { getErrorMsg } from '../../../constants/errorMessages';

import './ui-order-form.less';

class OrderFormOrganization extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object,
        errors: pt.object
    };

    render() {
        const { store, client } = this.context;
        const order = JSON.parse(JSON.stringify(this.props.order));
        const errors = this.props.errors[ERROR_ORDER] || [];

        return (
            <div>
                <form className='ui-order-form__form'>
                    <Field
                        id='org_name'
                        label='Название компании'
                        wide
                        value={order.company.name}
                        onChange={name => {
                            order.company.name = name;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='org_companyPhone'
                        label='Телефон'
                        wide
                        value={order.company.phone}
                        onChange={phone => {
                            order.company.phone = phone;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='org_inn'
                        label='ИНН'
                        wide
                        value={order.company.inn}
                        onChange={inn => {
                            order.company.inn = inn;
                            changeOrder(order, store, client);
                        }}
                        mask='999999999999'
                        error={getErrorMsg(errors, 'INN_ISNT_VALID')}
                    />
                    <Field
                        id='org_kpp'
                        label='КПП'
                        wide
                        value={order.company.kpp}
                        onChange={kpp => {
                            order.company.kpp = kpp;
                            changeOrder(order, store, client);
                        }}
                        mask='999999999'
                        error={getErrorMsg(errors, 'KPP_ISNT_VALID')}
                    />
                    <Field
                        id='org_okpo'
                        label='ОКПО'
                        wide
                        value={order.company.okpo}
                        onChange={okpo => {
                            order.company.okpo = okpo;
                            changeOrder(order, store, client);
                        }}
                        mask='9999999999'
                        error={getErrorMsg(errors, 'OKPO_ISNT_VALID')}
                    />
                    <Field
                        id='org_bik'
                        label='БИК'
                        wide
                        value={order.company.bik}
                        onChange={bik => {
                            order.company.bik = bik;
                            changeOrder(order, store, client);
                        }}
                        mask='049999999'
                        error={getErrorMsg(errors, 'BIK_ISNT_VALID')}
                    />
                    <Field
                        id='org_bank'
                        label='Банк'
                        wide
                        value={order.company.bank}
                        onChange={bank => {
                            order.company.bank = bank;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='org_factAddress'
                        label='Фактический адрес'
                        wide
                        value={order.company.factAddress}
                        onChange={factAddress => {
                            order.company.factAddress = factAddress;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='org_jurAddress'
                        label='Юридический адрес'
                        wide
                        value={order.company.jurAddress}
                        onChange={jurAddress => {
                            order.company.jurAddress = jurAddress;
                            changeOrder(order, store, client);
                        }}
                    />
                    <Field
                        id='org_raschSchet'
                        label='Расчётный счёт'
                        wide
                        value={order.company.raschSchet}
                        onChange={raschSchet => {
                            order.company.raschSchet = raschSchet;
                            changeOrder(order, store, client);
                        }}
                        mask='99999999999999999999'
                        error={getErrorMsg(errors, 'RASCH_ISNT_VALID')}
                    />
                    <Field
                        id='org_korrSchet'
                        label='Корр. счёт'
                        wide
                        value={order.company.korrSchet}
                        onChange={korrSchet => {
                            order.company.korrSchet = korrSchet;
                            changeOrder(order, store, client);
                        }}
                        mask='30199999999999999999'
                        error={getErrorMsg(errors, 'KORR_ISNT_VALID')}
                    />

                </form>
                <div className='ui-order-form__hint'>
                    На указанный телефон мы&nbsp;пришлём СМС с&nbsp;подтверждением заказа.
                    <br />
                    А&nbsp;когда посылка прибудет в&nbsp;пункт выдачи&nbsp;— оповестим о&nbsp;доставке.
                </div>
            </div>
        );
    }
}

// -----------------------------------------------------

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ order: state.order, errors: state.errors });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(OrderFormOrganization);
