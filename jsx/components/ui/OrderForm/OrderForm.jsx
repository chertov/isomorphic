import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { changeOrder } from 'api/order';

import { Tile } from '../Tile';
import { Title } from '../Title';
import { Checkbox } from '../Checkbox';
import OrderFormPR from './OrderFormPR.jsx';
// import OrderFormPickPoint from './OrderFormPickPoint.jsx';
// import OrderFormSamovyvoz from './OrderFormSamovyvoz.jsx';
import OrderFormOrganization from './OrderFormOrganization.jsx';
import OrderFormSubmit from './OrderFormSubmit.jsx';

import './ui-order-form.less';

import DELIVERY_TYPES from '../../../constants/deliveryTypes';

class OrderForm extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object
    };

    render() {
        const { store, client } = this.context;
        const order = Object.assign({}, this.props.order);
        const deliveryType = order.delivery.type || DELIVERY_TYPES.pochtaRossii.value; // TODO: Убрать Почту России

        if (!deliveryType) {
            return null;
        }

        return (
            <div>
                <Tile>
                    <Title tag='h3' className='ui-order-form__block-title'>
                        Заполните несколько полей
                    </Title>

                    {deliveryType === DELIVERY_TYPES.pochtaRossii.value && <OrderFormPR />}
                    {/* deliveryType === DELIVERY_TYPES.pickPoint.value && <OrderFormPickPoint />*/}
                    {/* deliveryType === DELIVERY_TYPES.samovyvoz.value && <OrderFormSamovyvoz />*/}

                    <div className='ui-order-form__checkbox-company'>
                        <Checkbox
                            id='isCompany_checkbox'
                            label='Заказ на юрлицо'
                            checked={order.isCompany}
                            onChange={checked => {
                                order.isCompany = checked;
                                changeOrder(order, store, client);
                            }}
                        />
                    </div>

                    {order.isCompany && <OrderFormOrganization />}
                </Tile>

                <OrderFormSubmit />
            </div>
        );
    }
}

// -----------------------------------------------------

// мапим состяние стора на props компонента
const mapStateToProps = state => ({ order: state.order });

// экспортируем компонент с замаппеным состянием и экшенами
export default connect(mapStateToProps)(OrderForm);
