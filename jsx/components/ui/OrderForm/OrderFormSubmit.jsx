import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import md5 from 'blueimp-md5';
import { makeOrder, ERROR_ORDER } from 'api/order';
import { loginMe } from 'api/me';

import { Tile } from '../Tile';
import { Button } from '../../ui/Button';
import { Field } from '../Field';
import { Loading } from '../Loading';

import './ui-order-form.less';

class OrderForm extends Component {
    static contextTypes = {
        store: pt.object,
        client: pt.object
    };

    static propTypes = {
        order: pt.object,
        orderlast: pt.object,
        errors: pt.object
    };

    state = { code: '' };

    handleSubmit = () => {
        const { store, client } = this.context;

        makeOrder(store, client, order => {
            this.setState({ showConfirm: true, order });
        });
    };

    handleConfirmCode = password => {
        const { store, client } = this.context;
        const { order } = this.state;

        this.setState({
            code: password,
            error: ''
        });

        const strarr = order.confimCode.split(':');
        const hash = strarr[0];
        const salt = strarr[1];
        const md5hashValue = md5(password + salt);

        if (md5hashValue === hash) {
            const passwordHash = md5(order.phone + password)

            loginMe(order.phone, passwordHash, store, client, () => {
                browserHistory.push({ pathname: `/ordersuccess/${order.number}`, state: { noScroll: false } });
            });
        } else {
            this.setState({ error: 'Неправильный пароль' });
        }
    };

    render() {
        const { code, error, order } = this.state;
        const errors = this.props.errors[ERROR_ORDER] || [];
        const showConfirm = !!order;

        return (
            <Tile className='ui-order-form__submit'>
                {showConfirm ?
                    <div>
                        <div className='ui-order-form__submit-text'>
                            Мы отправили СМС на номер {order.phone}. Введите пароль из СМС:
                        </div>
                        <div className='ui-order-form__submit-field-wrapper'>
                            <Field
                                id='confirm_code'
                                label='Пароль из СМС'
                                value={code}
                                onChange={this.handleConfirmCode}
                                autoFocus
                                error={error}
                            />
                        </div>
                        <Loading coverParent id={'SomeID'} /> {/* TODO: add actual id */}
                    </div> :
                    <div>
                        <div className='ui-order-form__submit-text'>
                            Проверьте правильность заполнения&nbsp;полей и&nbsp;нажимайте:
                        </div>
                        <Button
                            className='ui-order-form__submit-button'
                            disabled={errors.length > 0}
                            onClick={this.handleSubmit}
                        >
                            Оформить заказ
                        </Button>
                    </div>
                }
            </Tile>
        );
    }
}

// -----------------------------------------------------

// мапим состяние стора на props компонента
const mapStateToProps = state => ({ order: state.order, errors: state.errors, orderlast: state.orderlast });

// экспортируем компонент с замаппеным состянием и экшенами
export default connect(mapStateToProps)(OrderForm);
