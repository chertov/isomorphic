import React, { Component, PropTypes as pt } from 'react';
import classNames from 'classnames';

import './ui-tile.less';

class Tile extends Component {

    static propTypes = {
        margin: pt.oneOfType([pt.number, pt.string]),
        padding: pt.oneOfType([pt.number, pt.string]),
        tag: pt.string,
        relative: pt.bool,
        className: pt.string,
        children: pt.node,
        itemProp: pt.string, // schema.org
        itemScope: pt.bool, // schema.org
        itemType: pt.string // schema.org
    };

    static defaultProps = {
        tag: 'div'
    };

    render() {
        const { margin, padding, tag, relative, className, children, itemProp, itemScope, itemType } = this.props;
        const Tag = tag;

        return (
            <Tag
                className={classNames({
                    'ui-tile': true,
                    'ui-tile_relative': relative,
                    [className]: className
                })}
                style={{
                    margin,
                    padding
                }}
                itemProp={itemProp}
                itemScope={itemScope}
                itemType={itemType}
            >
                {children}
            </Tag>
        );
    }
}

export default Tile;
