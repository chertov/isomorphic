import React, { Component } from 'react';
import classNames from 'classnames';
import { Link as ReactRouterLink } from 'react-router';
import { Container } from '../ui/Container';
// import { Link } from '../ui/Link';
import { Auth } from '../ui/Auth';
import { Cart } from '../ui/Cart';

import './header.less';

class Header extends Component {

    state = { menuCollapsed: true };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        if (window.scrollY > 20 + 35 + 20) {
            this.setState({
                isSticky: true
            });
        } else if (window.scrollY < 20 + 35 + 20) {
            this.setState({
                isSticky: false
            });
        }
    };

    handleToggleMenu = () => {
        this.setState({
            menuCollapsed: !this.state.menuCollapsed
        });
    };

    handleCollapseMenu = () => {
        this.setState({
            menuCollapsed: true
        });
    };

    render() {
        const { menuCollapsed, isSticky } = this.state;


        return (
            <Container>
                <header className='header'>
                    <div className='header__left'>
                        <span className='header__logo-wrapper'>
                            <ReactRouterLink
                                className='ui-link header__logo'
                                activeClassName='header__logo_active'
                                onlyActiveOnIndex
                                title='На главную'
                                to='/'
                            >
                                Никамиста
                            </ReactRouterLink>
                            <div
                                className='ui-link ui-link_dashed header__logo-menu-toggle'
                                onClick={this.handleToggleMenu}
                            >
                                Меню
                            </div>
                        </span>

                        <nav
                            className={classNames({
                                header__links: true,
                                header__links_collapsed: menuCollapsed
                            })}
                            onClick={this.handleCollapseMenu}
                        >
                            <ul className='header__links-list'>
                                <li className='header__links-item'>
                                    <ReactRouterLink
                                        className='ui-link'
                                        activeClassName='ui-link_active'
                                        to='/about'
                                    >
                                        О продукции
                                    </ReactRouterLink>
                                </li>
                                <li className='header__links-item'>
                                    <ReactRouterLink
                                        className='ui-link'
                                        activeClassName='ui-link_active'
                                        to='/delivery'
                                    >
                                        Доставка и оплата
                                    </ReactRouterLink>
                                </li>
                                {/* this.props.userSignedIn &&
                                <li className='header__links-item'>
                                    <ReactRouterLink
                                        className='ui-link'
                                        activeClassName='ui-link_active'
                                        to='/my-orders'
                                    >
                                        Мои заказы
                                    </ReactRouterLink>
                                </li>
                                */}
                            </ul>
                        </nav>

                        <nav className='header__menu'>
                            <ul
                                className={classNames({
                                    'header__menu-list': true,
                                    'header__menu-list_collapsed': menuCollapsed
                                })}
                                onClick={this.handleCollapseMenu}
                            >
                                <li className='header__menu-item'>
                                    <ReactRouterLink
                                        className='ui-link header__menu-link'
                                        activeClassName='header__menu-link_active'
                                        to='/onlays'
                                    >
                                        Накладки Селиванова
                                    </ReactRouterLink>
                                </li>
                                <li className='header__menu-item'>
                                    <ReactRouterLink
                                        className='ui-link header__menu-link'
                                        activeClassName='header__menu-link_active'
                                        to='/catalog/onlays/common'
                                    >
                                        Для дома и офиса
                                    </ReactRouterLink>
                                </li>
                                <li className='header__menu-item'>
                                    <ReactRouterLink
                                        className='ui-link header__menu-link'
                                        activeClassName='header__menu-link_active'
                                        to='/catalog/onlays/automobile'
                                    >
                                        Автомобильные
                                    </ReactRouterLink>
                                </li>
                                {/*
                                <li className='header__menu-item'>
                                    <ReactRouterLink
                                        className='ui-link header__menu-link'
                                        activeClassName='header__menu-link_active'
                                        to='/armchairs'
                                    >
                                        Кресла и скамьи
                                    </ReactRouterLink>
                                </li>
                                <li className='header__menu-item'>
                                    <ReactRouterLink
                                        className='ui-link header__menu-link'
                                        activeClassName='header__menu-link_active'
                                        to='/massage'
                                    >
                                        Для салонов массажа
                                    </ReactRouterLink>
                                </li> */}
                            </ul>
                        </nav>
                    </div>

                    <div className='header__right' >
                        <div
                            className={classNames({
                                'header__right-strip': true,
                                'header__right-strip_sticky': isSticky
                            })}
                        >
                            <div
                                className='header__right-block'
                                itemScope itemType='http://schema.org/Organization'
                            >
                                <meta itemProp='name' content='Никамиста' />

                                {/* <Link className='header__phone' href='tel:+79101234567'>
                                    <span itemProp='telephone'>8 (910) 123‒45‒67</span>
                                </Link> */}

                                <div className='header__auth'>
                                    <noindex>
                                        <Auth />
                                    </noindex>
                                </div>
                            </div>

                            <div className='header__cart'>
                                <noindex>
                                    <Cart />
                                </noindex>
                            </div>
                        </div>
                    </div>
                </header>
            </Container>
        );
    }
}

export default Header;
