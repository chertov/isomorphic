import React, { Component } from 'react';
import { Link as ReactRouterLink } from 'react-router';
import { Container } from '../ui/Container';

import './footer.less';

class Footer extends Component {
    render() {
        return (
            <Container>
                <footer className='footer'>
                    <nav className='footer__nav'>
                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            onlyActiveOnIndex
                            to='/'
                        >
                            Главная
                        </ReactRouterLink>
                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            to='/about'
                        >
                            О продукции
                        </ReactRouterLink>
                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            to='/delivery'
                        >
                            Доставка и оплата
                        </ReactRouterLink>

                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            to='/onlays'
                        >
                            Накладки Селиванова
                        </ReactRouterLink>
                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            to='/catalog/onlays/common'
                        >
                            Для дома и офиса
                        </ReactRouterLink>
                        <ReactRouterLink
                            className='ui-link footer__link'
                            activeClassName='ui-link_active'
                            to='/catalog/onlays/automobile'
                        >
                            Автомобильные
                        </ReactRouterLink>
                    </nav>
                    <div className='footer__copyright'>
                        {/* © 2016–2017, «Никамиста», официальный сайт */}
                        © {new Date().getFullYear()}, «Никамиста», официальный сайт
                    </div>
                </footer>
            </Container>
        );
    }
}

export default Footer;
