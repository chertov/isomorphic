import React, { Component, PropTypes as pt } from 'react';
import { setHtmlTitle } from 'tools';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import Alert from '../ui/Alert/Alert.jsx';

import '../common/common.less';

class Layout extends Component {
    static propTypes = {
        children: pt.node
    };

    render() {
        const { children } = this.props;

        setHtmlTitle('Никамиста - магазин отропедических изделий');
        return (
            <div className='layout'>
                <Header />
                <div className='layout__content'>
                    {children}
                </div>
                <Footer />
                <Alert />
            </div>
        );
    }
}

export default Layout;
