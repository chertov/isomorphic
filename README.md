# README #
Для запуска сервиса необходимо склонировать в папку ```~/dev``` два репозитория
[__isomorphic__](https://bitbucket.org/chertov/isomorphic) и [__shop_backend__](https://bitbucket.org/chertov/shop_backend)

### Репозиторий [__isomorphic__](https://bitbucket.org/chertov/isomorphic) ###

Содержит в себе фронтенд и серверный рендер на базе react.js.

Собираются скрипты с помощью webpack первой версии.

Для запуска сборки скриптов необходимо запустить webpack в директории репозитория:
```sh
cd ~/dev/isomorphic
webpack --watch
```

### Репозиторий [__shop_backend__](https://bitbucket.org/chertov/shop_backend) ###

Содержит в себе сервер GraphQL на Golang и базу данных tarantool.

Для запуска сервисов необходимо собрать приложение Golang и запустить всю инфраструктуру в docker.
```sh
cd ~/dev/shop_backend
./build.sh
cd ~/dev/shop_backend/docker
docker-compose build && docker-compose up
```
При выполнении этих команд будет создано три docker-контейнера.
* Контейнер из репозитория [__isomorphic__](https://bitbucket.org/chertov/isomorphic) с серверным рендером.
* Контейнер из репозитория [__shop_backend__](https://bitbucket.org/chertov/shop_backend) с сервером GraphQL.
* Контейнер из репозитория [__shop_backend__](https://bitbucket.org/chertov/shop_backend) с базой данных tarantool.

После запуска контейнеров сервис будет доступен по адресу [http://127.0.0.1:3010/](http://127.0.0.1:3010/).

GUI для отладки GraphQL запросов доступно по адресу [http://127.0.0.1:3010/graphql](http://127.0.0.1:3010/graphql).

Прямой доступ к серверу GUI GraphQL по адресу [http://127.0.0.1:8080/](http://127.0.0.1:8080/).

При изменении папки ```~/dev/isomorphic/pub``` измененные данные автоматически доступны для контейнера и в перезапуске сервисов необходимости нет.

Но при необходимости изменить код ```~/dev/isomorphic/bin/server.js``` необходимо будет перезапустить сервис командой
```sh
docker-compose build && docker-compose up
```
в папке
```sh
cd ~/dev/shop_backend/docker
```