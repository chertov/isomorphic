
    import React from 'react';
    import ReactDOM from 'react-dom/server';
    import { Provider } from 'react-redux';
    import { RouterContext } from 'react-router';

    import ClientProvider from 'clientprovider.jsx';
    import meta from '../utils/meta';
    import { getTree, getTreeHash } from './global';
    import ResReqProvider from './resreqprovider.jsx';
    import YAMetrika from './yametrika.jsx';
    import GoogleAnalytics from './google.jsx';

    import getStore from './../jsx/store';
    import { getMeta } from './../jsx/tools';

    const renderHtml = (initStateValue, renderProps, req, res, client) => {
        const ati = ['57x57', '60x60', '72x72', '76x76', '114x114', '120x120', '144x144', '152x152', '180x180'];
        const icons = ['16x16', '32x32', '96x96'];

        const {
            metaDescription,
            metaKeywords,
            metaCanonical,
            metaTitle,
            metaOgTitle,
            metaTwitterTitle,
            metaTwitterDescription,
            metaOgSiteName,
            metaOgDescription,
            metaOgImage,
            metaOgUrl
        } = meta();

        const store = getStore(Object.assign({}, initStateValue, { tree: getTree() }));

        res.checkHeaders = () => res.headersSent;

        const htmlTemplate = <html lang='ru'>
            <head>
                <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
                <meta httpEquiv='Content-Type' content='text/html; charset=utf-8' />
                <meta charSet='UTF-8' />
                <title>{'metaTitle_ITS_A_STRING'}</title>
                <meta name='description' content={metaDescription} data-dynamic='true' />
                <meta name='keywords' content={metaKeywords} data-dynamic='true' />
                <link rel='canonical' href={metaCanonical} data-dynamic='true' />
                <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />

                <meta property='og:title' content={metaOgTitle} data-dynamic='true' />
                <meta property='og:description' content={metaOgDescription} data-dynamic='true' />
                <meta property='og:url' content={metaOgUrl} data-dynamic='true' />
                <meta property='og:type' content='website' />
                <meta property='og:image' content={metaOgImage} data-dynamic='true' />
                <meta property='og:site_name' content={metaOgSiteName} data-dynamic='true' />
                <meta property='twitter:title' content={metaTwitterTitle} data-dynamic='true' />
                <meta property='twitter:description' content={metaTwitterDescription} data-dynamic='true' />

                <meta name='msapplication-TileColor' content='#ffdd2d' />
                <meta name='msapplication-TileImage' content='/mstile-144x144.png' />
                <meta name='theme-color' content='#ffdd2d' />
                <meta name='robots' content='all' />
                <meta name='robots' content='index, follow' />

                {/* Marketing, vol. 1 */}
                {/* this.errorLoggerScript()*/}
                {/* this.trackingRequestsScript()*/}


                {/* process.env.NODE_ENV === 'production' && this.inlineGlobalScript(`var webpackManifest=${JSON.stringify(this.props.manifest)}`)*/}

                {/* <script
                    src={config.getBundle(buildEntry, 'js')}
                    defer='defer'
                    nonce={this.props.cspToken}
                    id='platformJS'
                />*/}

                <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic' rel='stylesheet' />
                <link href='https://fonts.googleapis.com/css?family=Yeseva+One&amp;subset=cyrillic' rel='stylesheet' />
                <link href='/css/style.css' rel='stylesheet' />

                {/* <link rel='stylesheet' href={config.getBundle(buildEntry, 'css')} nonce={this.props.cspToken} id='platformCSS' />*/}
                {ati.map((size, index) => <link rel='apple-touch-icon' key={size} sizes={size} href={`/apple-touch-icon-${size}.png`} />)}
                {icons.map((size, index) => <link rel='icon' key={size} sizes={size} href={`/favicon-${size}.png`} />)}
                <link rel='android-touch-icon' type='image/png' href='/android-chrome-192x192.png' sizes='192x192' />
                <link rel='icon' type='image/png' href='/android-chrome-192x192.png' sizes='192x192' />
                {/* <link rel='icon' href={config.getAsset('images/favicon.ico')} type='image/x-icon' />*/}
                <link rel='manifest' href='/manifest.json' />
                <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#333333' />
            </head>
            <body>
                <YAMetrika />
                <GoogleAnalytics />
                <div id='root'>
                    <ResReqProvider req={req} res={res}>
                        <ClientProvider client={client}>
                            <Provider store={store}>
                                <RouterContext {...renderProps} />
                            </Provider>
                        </ClientProvider>
                    </ResReqProvider>
                </div>
                <div id='ym' />
                <script dangerouslySetInnerHTML={{ __html: `windowStateValue=${JSON.stringify(initStateValue)};` }} />
                <script dangerouslySetInnerHTML={{ __html: `treeHash=${getTreeHash()};` }} />
                <script src='/js/bundle.js' />
            </body>
        </html>;
        let htmlStr = `<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlTemplate)}`;

        if (res !== null && !res.headersSent) {
            const meta = getMeta();

            htmlStr = htmlStr.replace('metaTitle_ITS_A_STRING', meta.title);
            res.status(200);
            res.send(htmlStr);
            res.end();
            return true;
        }
        return htmlTemplate;
    };

    export default renderHtml;
