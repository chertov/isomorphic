
    import ApolloClient, { createNetworkInterface } from 'apollo-client';
    import { graphqlHost } from './../jsx/tools';

    export const serviceClient = new ApolloClient({
        ssrMode: false,
        // addTypename: false,
        // Remember that this is the interface the SSR server will use to connect to the
        // API server, so we need to ensure it isn't firewalled, etc
        // connectToDevTools: true,
        networkInterface: createNetworkInterface({
            uri: `${graphqlHost()}query`,
            opts: {
                credentials: 'same-origin',
                headers: {
                    Cookie: 'sid=admin_cb9c1a5b'
                }
            }
        })
    });

    export const userClient = sid => new ApolloClient({
        ssrMode: false,
        // addTypename: false,
        // Remember that this is the interface the SSR server will use to connect to the
        // API server, so we need to ensure it isn't firewalled, etc
        // connectToDevTools: true,
        networkInterface: createNetworkInterface({
            uri: `${graphqlHost()}query`,
            opts: {
                credentials: 'same-origin',
                headers: {
                    Cookie: `sid=${sid}`
                }
            }
        })
    });
