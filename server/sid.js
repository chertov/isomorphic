
    import 'isomorphic-fetch';
    import { graphqlHost } from './../jsx/tools';

    const checkSid = true;
    const maxAge = 5 * 12 * 31 * 24 * 60 * 60 * 1000; // срок хранения кук sid 5 лет

    export default function(router) {
        router.use((req, res, next) => {
            if (!checkSid) { next(); return; }
            // запрашиваем проверку идентификатора сессии
            fetch(`${graphqlHost()}sid/${req.cookies.sid}`)
            .then(response => response.text())
            .then(sid => {
                if (sid === 'ok') { // в случае если сессия валидна
                    req.sid = req.cookies.sid;
                    next();
                } else {
                    // если сессия была не валидна,то сохраняем новый идентификатор в куки
                    // и делаем редирект для подтверждения
                    console.log(`The new sid is '${sid}'`); // redirect to confirm on ${req.url}`);
                    res.cookie('sid', sid, { maxAge, httpOnly: true });
                    req.sid = sid;
                    // res.redirect(req.url);
                    next();
                }
            })
            .catch(errorClient => {
                // console.log('Can\'t check sid', errorClient);
                // res.status(200);
                // res.send(errorClient);
                // res.end();

                next();
            });
        });
    }
