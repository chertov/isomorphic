
    import Route from 'route-parser';
    import React from 'react';
    import renderHtml from './../html.jsx';
    /* eslint no-console: 0*/

    const routeStr = '/cart';
    const route = new Route(routeStr);

    export default function(req, res, renderProps) {
        const params = route.match(req.url);

        if (params === false) { return false; }

        console.log('/cart req.sid:', req.sid, '   url: ', req.url);
        console.log('params:', params);

        const initStoreValue = { counter: 373 };
        return renderHtml(initStoreValue, renderProps, res);
    }
