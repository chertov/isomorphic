
    import Route from 'route-parser';
    import React from 'react';
    import renderHtml from './../html.jsx';
    import { getTree } from './../global';
    /* eslint no-console: 0*/

    const routeStr = '/catalog(/:c0)(/:c1)(/:c2)(/:c3)(/:c4)(/:c5)(/:c6)(/:c7)(/:c8)(/:c9)'
        + '(/:c10)(/:c11)(/:c12)(/:c13)(/:c14)(/:c15)(/:c16)(/:c17)(/:c18)(/:c19)';
    const route = new Route(routeStr);

    export default function(req, res, renderProps) {
        const params = route.match(req.url);

        if (params === false) { return false; }
        console.log('/catalog req.sid:', req.sid, '   url: ', req.url);
        console.log('params:', params);

        const initStoreValue = {
            tree: getTree(),
            counter: 373
        };

        return renderHtml(initStoreValue, renderProps, res);
    }
