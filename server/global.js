
    import { getHash } from 'tools';
    import { getTreeQuery } from 'sagas/catalog/gettree';
    import { serviceClient } from './graphql';

    let tree = {};
    let treeHash = '';

    export const getTree = () => tree;
    export const getTreeHash = () => treeHash;

    const updateGlobal = () => {
        try {
            getTreeQuery(serviceClient)
            .then(response => {
                tree = Object.assign({}, response);
                const foldersMap = {};
                const categoriesMap = {};
                const productsMap = {};
                const modelsMap = {};

                tree.root = { folders: tree.rootfolders, categories: tree.rootcategories };
                delete tree.rootfolders;
                delete tree.rootcategories;
                tree.folders.forEach(folder => foldersMap[folder.id] = folder);
                tree.folders = foldersMap;
                tree.categories.forEach(category => categoriesMap[category.id] = category);
                tree.categories = categoriesMap;
                tree.products.forEach(product => productsMap[product.id] = product);
                tree.products = productsMap;
                tree.models.forEach(model => modelsMap[model.id] = model);
                tree.models = modelsMap;
                treeHash = getHash(JSON.stringify(tree));
            })
            .catch(error => console.log('error:', error));
        } catch (error) {
            console.log('tree error:', error);
        }
    };

    export default function() {
        setTimeout(updateGlobal, 1000);
        setInterval(updateGlobal, 10000);
    }
