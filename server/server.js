import 'source-map-support/register';

import os from 'os';
import http from 'http';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import React from 'react';
import { match } from 'react-router';
import proxy from 'express-http-proxy';

import routes from './../jsx/router.jsx';

import { onserver, graphqlHost } from './../jsx/tools';
import initAuth from './auth';
import initSidCechker from './sid';
import renderHtml from './html.jsx';
import getInitState from './pages';

import { userClient } from './graphql';
import { getOrderQuery } from './../jsx/sagas/order/get';
import { getMeQuery } from './../jsx/sagas/me';
import initGlobal, { getTree } from './global';
/* eslint no-console: 0*/

console.log(`On ${os.platform()} server rendering: `, onserver());

initGlobal();

const app = express();

app.use((req, res, next) => {
    const host = req.headers.host;

    if (host === 'ezhiki-klubochki.ru' || host === 'www.ezhiki-klubochki.ru') {
        res.redirect(301, 'https://vk.com/ezhiki_klubochki');
        return;
    }
    if (host !== 'nikamista.com' && host !== '127.0.0.1:3010' && host !== 'localhost:3010' && host !== '127.0.0.1:3011') {
        res.redirect(301, 'http://nikamista.com/');
        return;
    }
    next();
});

app.use(cookieParser());

app.use('/android*', (req, res, next) => { res.status(200); res.send(''); res.end(); });
app.use('/favicon*', (req, res, next) => { res.status(200); res.send(''); res.end(); });

// проксируем запросы graphql на сервер
app.use('/graphql', proxy(graphqlHost()));
app.use('/query', proxy(graphqlHost(), { forwardPath: (req, res) => '/query' }));

// отдаем статику
const publicPath = path.join(path.dirname(process.mainModule.filename), './../pub');

console.log('public_path', publicPath);
// app.use(express.static(public_path))
app.use('/js', express.static(`${publicPath}/js`));
app.use('/images', express.static(`${publicPath}/images`));
app.use('/favicon.ico', express.static(`${publicPath}/favicon.ico`));
app.use('/robots.txt', express.static(`${publicPath}/robots.txt`));
app.use('/css', express.static(`${publicPath}/css`));
app.use('/manifest.json', (req, res, next) => {
    res.status(200);
    res.send('{}');
    res.end();
});
app.use('/tree.json', (req, res, next) => {
    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(getTree()));
    res.end();
});

const router = express.Router();

initSidCechker(router);
initAuth(router);

router.get('*', (req, res) => {
    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
        if (error) {
            res.status(500).send(error.message);
        } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search);
        } else if (renderProps) {
            // console.log('req.sid:', req.sid, '   url: ', req.url);
            const initStore = getInitState(renderProps);
            const client = userClient(req.sid);

            getMeQuery(client).then(me => {
                initStore.me = me;
                getOrderQuery(client).then(order => {
                    initStore.order = order;
                    renderHtml(initStore, renderProps, req, res, client);
                });
            });
        } else {
            res.status(404).send('Not found');
        }
    });
});

app.use('/', router);
const server = http.createServer(app);
let port = 3010;

if (os.platform() === 'win32') { port = 3011; }
server.listen(port, '0.0.0.0', () => {
    console.log(`Example app listening on port ${server.address().address}:${server.address().port}`);
});
