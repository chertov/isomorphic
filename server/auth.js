
    import { client } from './graphql';

    export default function(app) {
        // редирект от гугла после авторизации..
        app.get('/google_auth_redirect', (req, res) => {
            // делаем запрос к API чтобы подтвердить авторизацию и получить текущего пользователя
            client.query(`
                query{
                    auth{
                        google{
                            auth(code:$code) {
                                id
                                name
                                email
                            }
                        }
                    }
                }
            `, { code: req.query.code })
            .then(data => {
                const user = data.auth.google.auth;

                res.send(user);
            })
            .catch(error => {
                res.send(`<html>
                    <title>Golang Auth Google Example</title>
                    <body>
                        Google Auth:<br />
                        <p>${error}</p>
                    </body>
                </html>`);
            });
        });

        // страница логина через Google
        // Пример получения URL для запроса авторизации
        app.get('/google_login', (req, res) => {
            client.query(`
                query{
                    auth{
                        google{
                            loginurl
                        }
                    }
                }
            `)
            .then(data => {
                const url = data.auth.google.loginurl;

                res.send(`<html>
                    <title>Golang Login Google Example</title>
                    <body>
                        <a href='${url}'>
                            <button>Login with Google!</button>
                        </a>
                    </body>
                </html>`);
            })
            .catch(error => {
                res.send(`<html>
                    <title>Golang Login Google Example</title>
                    <body>
                        Google Login:<br />
                        <p>${error}</p>
                    </body>
                </html>`);
            });
        });
    }
