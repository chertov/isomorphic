
    import React, { Component, PropTypes as pt } from 'react';

    class ResReqProvider extends Component {
        static propTypes = {
            req: pt.object,
            res: pt.object
        }
        static childContextTypes = {
            req: pt.object,
            res: pt.object
        }
        getChildContext() {
            return {
                req: this.props.req,
                res: this.props.res
            };
        }
        render() {
            return this.props.children;
        }
    }

    export default ResReqProvider;
