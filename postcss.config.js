/* eslint-disable import/no-commonjs, global-require */

const autoprefixer = require('autoprefixer');
const csso = require('postcss-csso');

module.exports = {
    plugins: [
        csso({ restructure: false, comments: false }),
        autoprefixer({ browsers: ['> 5%', 'Firefox >= 28', 'Explorer >= 11', 'Chrome >= 28', 'Opera >= 32', 'Safari >= 6', 'Edge >= 20'] })
    ]
};
