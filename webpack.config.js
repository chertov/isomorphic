
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const combineLoaders = require('webpack-combine-loaders');

const browser = {
    name: 'Browser',
    entry: './jsx/app.jsx',
    output: {
        path: './pub/js',
        filename: 'bundle.js'
    },

    stats: { children: false }, // prevent spam-logs in console
    watch: true,
    cache: true,
    debug: true,
    devtool: 'source-map',
    resolve: {
        root: [path.resolve(__dirname, 'jsx'), path.resolve(__dirname, 'node_modules')],
        extensions: ['', '.js', '.jsx', 'json']
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.less$/,
                // loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize=false!csso-loader?-comments!postcss-loader!less-loader?-compress')
                // loader: ExtractTextPlugin.extract('style-loader',
                // 'css-loader?sourceMap!'
                // + 'csso-loader?sourceMap!'
                // + 'postcss-loader?sourceMap!'
                // + 'less-loader?sourceMap')
                loader: ExtractTextPlugin.extract('style-loader',
                    combineLoaders([
                        { loader: 'css-loader', query: { sourceMap: true, importLoaders: 1, minimize: false } },
                        // { loader: 'csso-loader', query: { sourceMap: true } },
                        { loader: 'postcss-loader', query: { sourceMap: true } },
                        { loader: 'less-loader', query: { sourceMap: true } }
                    ])
                )
            },
            { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /\.json$/, loader: 'json-loader' }
        ]
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
        new ExtractTextPlugin('./../css/style.css'),
        // new webpack.optimize.DedupePlugin(),
        // new webpack.optimize.UglifyJsPlugin(),
        new WebpackNotifierPlugin({ excludeWarnings: true, alwaysNotify: true }),
        new ProgressBarPlugin(),
        new CircularDependencyPlugin({
          // add errors to webpack instead of warnings
          failOnError: true
        })
    ]
};

const server = {
    name: 'Server',
    entry: './server/server.js',
    output: {
        path: './bin',
        filename: 'server.js'
    },
    target: 'node',

    stats: { children: false },
    watch: true,
    cache: true,
    debug: true,
    devtool: 'source-map',
    resolve: {
        root: [path.resolve(__dirname, 'jsx'), path.resolve(__dirname, 'server'), path.resolve(__dirname, 'node_modules')],
        extensions: ['', '.js', '.jsx', 'json']
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: 'ignore-loader' },
            { test: /\.less$/, loader: 'ignore-loader' },
            { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /\.json$/, loader: 'json-loader' }
        ]
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
        // new webpack.optimize.DedupePlugin(),
        // new webpack.optimize.UglifyJsPlugin(),
        new WebpackNotifierPlugin({ excludeWarnings: true, alwaysNotify: true }),
        new ProgressBarPlugin()
    ]
};

module.exports = [browser, server];
